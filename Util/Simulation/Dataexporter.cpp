/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "Dataexporter.h"

namespace Simulation
{

  static void getCudaError()
  {
    cudaError err = cudaGetLastError();
    if(cudaSuccess != err)
    {
      std::cout << "An error occured in the kernel..." << '\n';
      std::cout << cudaGetErrorString( err ) << '\n';
    }
  }


  template<class T>
  Dataexporter<T>::Dataexporter(int Lx, int Ly, int Lz, int start, int amount, int pitch, int xVals, int diff, int saveDiff, T** cuda_data, std::string title, int index)
  {
    this->Lx = Lx;
    this->Ly = Ly;
    this->Lz = Lz;

    this->start = start;
    this->amount = amount;
    this->pitch = pitch;
    this->diff = diff;
    this->saveDiff = saveDiff;

    this->xVals = xVals;

    this->cuda_data = cuda_data;
    this->title = title;

    this->index = index;

    //The array advancement index is set to zero by standard.
    cIndex = 0;

    //Construct the data array.
    data = new T*[saveDiff];

    for(int i = 0; i < saveDiff; i++)
    {
      data[i] = new T[amount*xVals];
    }
  }

  template<class T>
  Dataexporter<T>::~Dataexporter()
  {
    //Free the memory of the data array.
    for(int i = 0; i < saveDiff; i++)
    {
      delete[] data[i];
    }

    delete[] data;
  }

  template<class T>
  void Dataexporter<T>::update(int timeStep)
  {
    //Only grab data at intervals.
    if(timeStep % diff == 0)
    {
	    cIndex = (cIndex+1)%saveDiff;

      //Grab the data, TODO: Not sure about second argument, begin error searching here if something isnt right.
      if(cudaMemcpy2D(data[cIndex], sizeof(T)*xVals, *cuda_data+start, sizeof(T)*pitch, sizeof(T)*xVals, amount, cudaMemcpyDeviceToHost) != cudaSuccess)
      {
        std::cout << "Failed to copy memory, error for: " << title << '\n';
        std::cout << "Memory position failed at value: " << start*sizeof(T) << '\n';
        getCudaError();
      }

      //Since the operations are serialized ths command shouldn't be needed but I am including it anyway.
      cudaDeviceSynchronize();

      //Save at the correct timestep, this is an additional overhead that is acceptable.
      if(cIndex == 0 && timeStep != 0)
      {
	      //The indexing are in different systems since the simulation starts from zero.
        saveFile(timeStep+diff);
      }

    }
  }

  template<class T>
  void Dataexporter<T>::dump(int timeStep)
  {

  }

  template<class T>
  void Dataexporter<T>::changeTitle(std::string title)
  {
    this->title = title;
  }

  template<class T>
  void Dataexporter<T>::saveFile(int timeStep)
  {
    std::ofstream dumpFile;
    std::string fileName = title;
    dumpFile.open(fileName.c_str());

    for(int j = 0; j < saveDiff; j++)
    {

      for(int k = 0; k < amount; k++)
      {
        dumpFile << data[j][k] << " ";
      }

      dumpFile << std::endl;
    }

    dumpFile.close();
  }



  //The function specifications, needed for the compiler to be able to work its magic.
  template Dataexporter<float>::Dataexporter(int Lx, int Ly, int Lz, int start, int amount, int pitch, int xVals, int diff, int saveDiff, float** cuda_data, std::string title, int index);
  template Dataexporter<double>::Dataexporter(int Lx, int Ly, int Lz, int start, int amount, int pitch, int xVals, int diff, int saveDiff, double** cuda_data, std::string title, int index);

  template Dataexporter<float>::~Dataexporter();
  template Dataexporter<double>::~Dataexporter();

  template void Dataexporter<float>::update(int timeStep);
  template void Dataexporter<double>::update(int timeStep);

  template void Dataexporter<float>::dump(int timeStep);
  template void Dataexporter<double>::dump(int timeStep);

  template void Dataexporter<float>::changeTitle(std::string title);
  template void Dataexporter<double>::changeTitle(std::string title);

  template void Dataexporter<float>::saveFile(int timeStep);
  template void Dataexporter<double>::saveFile(int timeStep);

}

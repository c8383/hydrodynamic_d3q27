/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "Logger.h"

namespace Logging
{
  Logger::Logger(std::string fileName)
  {
    this->logName = fileName;

    //Create the file.
    std::fstream file;
    file.open(this->logName,std::fstream::out);
    file << "Log created: " << std::endl;
    file.close();

    //Get the current time.
    start = std::chrono::system_clock::now();
  }

  Logger::~Logger()
  {
    //Nothing now.
  }

  void Logger::printToFile(std::string text)
  {
    std::fstream file;
    file.open(this->logName,std::ios_base::app);
    file << text << std::endl;
    file.close();
  }

  void Logger::endSweep(int iterations, int number, float Re, float visc, float avgVel)
  {
    //Print all stuff relevant to this sweep.
    std::fstream file;
    file.open(this->logName,std::ios_base::app);
    file << "-----------------------------------------------------------------------------------------------" << std::endl;
    file << "Sweep ended at " << (std::chrono::system_clock::now() - start).count() << std::endl;
    file << "Nr iterations: " << iterations << std::endl;
    file << "Sweep number: " << number << std::endl;
    file << "Reynolds number is: " << Re << std::endl;
    file << "Visc is: " << visc << std::endl;
    file << "Avg vel is: " << avgVel << std::endl;

    file.close();
  }

  void Logger::endSession()
  {
    //Print all stuff relevant to this sweep.
    std::fstream file;
    file.open(this->logName,std::ios_base::app);
    file << "-----------------------------------------------------------------------------------------------" << std::endl;
    file << "Simulation is now complete." << std::endl;
    file << "-----------------------------------------------------------------------------------------------" << std::endl;
    file.close();
  }

}

/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <cuda_runtime.h>
#include <iostream>
#include <string>
#include <fstream>

namespace Simulation
{
  /**
   * This class exports data along a line or in only one point
   * according to the specifications from the user.
   * @param Lx        X-Length of simulation block
   * @param Ly        Y-Length of simulation block
   * @param Lz        Z-Length of simulation block
   * @param start     Array index where saving should start
   * @param end       Array index where saving should end
   * @param diff      Difference in global time-step between consecutive data extractions
   * @param saveDiff  Difference in data extractions to file savings in terms of global time step
   * @param cuda_data The pointer to the cuda memory address
   */
  template<class T>
  class Dataexporter
  {
  public:
    Dataexporter<T>(int Lx, int Ly, int Lz, int start, int amount, int pitch, int xVals, int diff, int saveDiff, T** cuda_data, std::string title, int index);
    ~Dataexporter<T>();

    /**
     * Gets the data specified in the construtor and saves to a file if it has reached the diff
     * value. For example if you are only saving for one point a high diff value is recommended.
     * @param timeStep The global timestep
     */
    void update(int timeStep);

    /**
     * Is called when the simulation ends either by expected termination or
     * @param timeStep The global timestep
     */
    void dump(int timeStep);

    /**
     * Changes the title of the file.
     * @param title The new title.
     */
    void changeTitle(std::string title);

    int index;

  private:
    //This contains both time and spatial components.
    std::string title;
    T** data;
    T** cuda_data;
    int Lx, Ly, Lz;
    int start, amount, pitch, diff, saveDiff, cIndex, xVals;

    /**
     * Saves a file with a filename specified by the string and the current timestep.
     * @param timeStep The global simulation timestep.
     */
    void saveFile(int timeStep);
  };
}

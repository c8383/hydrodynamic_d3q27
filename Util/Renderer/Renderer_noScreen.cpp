/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "Renderer.h"
#include <cstring>


namespace Render
{
  //We also have an fbo variable.
  GLuint fbo;
  GLuint rbo;
  GLuint tex_color_buffer;

  WindowParams::WindowParams(int width, int height, char* title)
  {
    //Set values equal to.
    this->height = height;
    this->width = width;
    this->title = title;
  }

  WindowParams::~WindowParams()
  {
    //Free the title.
    delete[] title;
  }

  GeometryParams::GeometryParams(int Lx, int Ly, int Lz, float dx, float dy, float dz)
  {
    this->Lx = Lx;
    this->Ly = Ly;
    this->Lz = Lz;

    this->dx = dx;
    this->dy = dy;
    this->dz = dz;
  }


  Renderer::Renderer(WindowParams *wPar, GeometryParams *gPar, Simulation::SimulationProperties *simPropin,
                              char* type)
  {

    this->wPar = wPar;
    this->gPar = gPar;
    this->type = type;
    glfwSetup();
    GLSetup();
    geomSetUp();

    //Set the simulation properties equal to the given one.
    simProp = simPropin;

    //Set up the callback functions.
    glfwSetKeyCallback(this->window, callBackKeyboard);
    glfwSetMouseButtonCallback(this->window, callBackMouseClick);
    glfwSetCursorPosCallback(this->window, callBackMouseMotion);
    glfwSetWindowSizeCallback(this->window, callBackOnResize);

  }

  Renderer::~Renderer()
  {
    glfwDestroyWindow(window);

    //Delete the buffer data.
    glDeleteBuffers(1,&vboGeom);

    //The window parameters are no longer necessary, free them.
    delete wPar;
  }

  void Renderer::pollEvents()
  {
    //Call the event poller.
    glfwPollEvents();

    //Change the renderered object.
    if(ChangeRenderOpt)
    {
      ChangeRenderOpt = false;

      if(renderObjects.size() < 2)
      {
        std::cout << "There is only one rendercontext set-up!" << '\n';
      }
      else
      {
        for(int i = 0; i < renderObjects.size(); i++)
        {
          if(renderObjects[i]->on)
          {
            renderObjects[i]->on = false;
            renderObjects[(i+1)%renderObjects.size()]->on = true;
            break;
          }
        }
      }
    }
  }

  void Renderer::view()
  {
    //Clear the previous color buffer.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Do the matrix operations and upload the resulting matrix.
    View = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f,0.0f,dist_z));
    View = glm::rotate(View, rotate_x, glm::vec3(0.0f,1.0f,0.0f));
    View = glm::rotate(View, rotate_y, glm::vec3(cos(6.28f*rotate_x/(360.0f)),sin(6.28f*rotate_x/(360.0f)),0.0f));

    //Change the projection matrix.
    glfwGetWindowSize(this->window, &wPar->width, &wPar->height);

    Projection = glm::ortho(-1000.0f*((float)wPar->width)/((float)wPar->height)/dist_z, 1000.0f*((float)wPar->width)/((float)wPar->height)/dist_z, -1000.0f/dist_z, 1000.0f/dist_z, 0.001f, 1000.0f);


    glm::mat4 MVP = Projection * View * Model;

    //Loop over all entries in the vector and render.
    for(auto renderObject : renderObjects)
    {
      if(renderObject->on)
      {
        renderObject->program->bindProgram();

        glUniformMatrix4fv(renderObject->mvpAttrib, 1, GL_FALSE, &MVP[0][0]);
        glUniform1f(renderObject->strengthAttrib, (simProp->renderInt));

        //Bind the vertex array and draw it as points for now.
        renderObject->bindVao();

        if(renderObject->positional)
        {
          glPointSize(2.0f); //Adjust the point size to avoid crystal effect.
        }
        else
        {
          glPointSize(2.0f);
        }

        //Enable the attribute arrays.
        renderObject->enableAttributes();
        glDrawArrays(GL_POINTS, 0, renderObject->size);
        renderObject->disableAttributes();
        glBindVertexArray(0);

        renderObject->program->unbindProgram();
      }
    }

    glfwSwapBuffers(window);
  }


  //This function passes a predefined transformation matrix.
  void Renderer::view(float rot_top, float rot_down, float dist, float scale)
  {
    
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glViewport(0, 0, wPar->width, wPar->height);
    //This is basically exactly what the simpleGL cuda code does at the moment.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Do the matrix manipulations and upload the resulting matrix.
    //This could be placed in a separate function to add some abstraction.
    View = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f,0.0f,-dist));
    View = glm::rotate(View, rot_top, glm::vec3(0.0f,0.0f,1.0f));
    View = glm::rotate(View, rot_down, glm::vec3(-cos(rot_top),sin(rot_top),0.0f));

    //std::cout << wPar->width << " " << wPar->height << std::endl;

    //Change the projection matrix.
    //glfwGetWindowSize(this->window, &wPar->width, &wPar->height);
    Projection = glm::ortho(-1.0f*((float)wPar->width)/((float)wPar->height)*scale, 1.0f*((float)wPar->width)/((float)wPar->height)*scale, -1.0f*scale, 1.0f*scale, 0.001f, 10000.0f);


    glm::mat4 MVP = Projection * View * Model;

    //Loop over all entries in the vector and render.
    for(auto renderObject : renderObjects)
    {
      if(renderObject->on)
      {
        renderObject->program->bindProgram();

        glUniformMatrix4fv(renderObject->mvpAttrib, 1, GL_FALSE, &MVP[0][0]);
        glUniform1f(renderObject->strengthAttrib, (simProp->renderInt));


        //Bind the vertex array and draw it as points for now.
        renderObject->bindVao();

        //Enable the attribute arrays.
        renderObject->enableAttributes();
        glDrawArrays(GL_POINTS, 0, renderObject->size);
        renderObject->disableAttributes();
        glBindVertexArray(0);

        renderObject->program->unbindProgram();
      }
    }

    //glfwSwapBuffers(window);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  bool Renderer::glfwSetup()
  {
    if(!glfwInit())
    {
      std::cout << "Could not initialize GLFW3!" << '\n';
      return false;
    }

    //Null for windowed mode, resources will not be shared with another
    //window and therefore the share is also Null. Also set this as the current OpenGL context.
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);     //Make the glfwWindow invisible.
    window = glfwCreateWindow(wPar->width, wPar->height, wPar->title, NULL, NULL);
    glfwMakeContextCurrent(window);

    const GLenum err = glewInit();

    if(err != GLEW_OK)
    {
      std::cout << "Failed to Initialize GLEW! exiting" << '\n';
      std::cout << "GLEW ERROR: " << glewGetErrorString(err) << std::endl;
      return -1;
    }

    //Also remove the 60 FPS cap.
    //glfwSwapInterval(0);


    
    return true;
  }

  bool Renderer::GLSetup()
  {

    //Create the fbo.
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);


    //Create the tex color buffer, necessary for some reason.
    glGenTextures(1, &tex_color_buffer);
    glBindTexture(GL_TEXTURE_2D, tex_color_buffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, wPar->width, wPar->height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_color_buffer, 0);

    //Create the rbo.
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, wPar->width, wPar->height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Framebuffer is not complete!" << std::endl;
        return -1;
    }


    glClearColor(0.0,0.0,0.0,1.0);
    //glEnable(GL_DEPTH_TEST); //Make stuff at front appear at front.
    glEnable(GL_ALPHA_TEST);
    glEnable (GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
    glBlendEquation(GL_FUNC_ADD);
    //glBlendEquation(GL_MAX);
    glViewport(0, 0, wPar->width, wPar->height);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //Set up the matrices, the view matrix is recalculated every time.
    Projection = glm::perspective<float>(glm::radians(45.0f), 4.0 / 4.0f, 0.1f, 10000.f);
    Model = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f));

    //The model matrix moves the model into the center.
    Model = glm::translate(Model, glm::vec3(-gPar->dx*((float)(gPar->Lx/2)),
                                            -gPar->dy*((float)(gPar->Ly/2)),
                                            -gPar->dz*((float)(gPar->Lz/2))));

    return true;
  }


  bool Renderer::geomSetUp()
  {

    //use the geometry properties to generate and upload the data to the GPU.
    float* geomVals = new float[(gPar->Lx)*(gPar->Ly)*(gPar->Lz)*3];
    int ind;

    for(int i = 0; i < gPar->Lx; i++)
    {
      for(int j = 0; j < gPar->Ly; j++)
      {
        for(int k = 0; k < gPar->Lz; k++)
        {
          ind = i+(gPar->Lx)*j+(gPar->Lx)*(gPar->Ly)*k;

          geomVals[ind*3] = gPar->dx*((float) i);
          geomVals[ind*3+1] = gPar->dy*((float) j);
          geomVals[ind*3+2] = gPar->dz*((float) k);
        }
      }
    }

    //Remember to binf fbo first.
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    //Generate and upload buffers.
    glGenBuffers(1,&vboGeom);
    glBindBuffer(GL_ARRAY_BUFFER, vboGeom);
    glBufferData(GL_ARRAY_BUFFER, gPar->Lx*gPar->Ly*gPar->Lz*3*sizeof(float), geomVals, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    //Free the local memory.
    delete[] geomVals;

    //No way to fail yet, return.
    return true;
  }

  void Renderer::addRenderObject(RenderObject* renderObject)
  {

    //Remember to bind fbo first when creating additional GPU stuff.
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    //Add it to the vector of renderobjects.
    renderObjects.push_back(renderObject);

    //Set up the VAO.
    renderObject->bindVao();

    //First set the positions (these are always floats)
    glBindBuffer(GL_ARRAY_BUFFER, vboGeom);
    glVertexAttribPointer(renderObject->posAttrib,3,GL_FLOAT,GL_FALSE,0,0);

    //Now set up the attributes.
    renderObject->defineVao();

    //Disable the attributes afterwards.
    renderObject->disableAttributes();

    //Also disable the vertex array.
    glBindVertexArray(0);


    glBindFramebuffer(GL_FRAMEBUFFER, 0);

  }

  void Renderer::dumpScreen(std::string fileName)
  {

    // Allocate memory for RGB data
    unsigned char* pDat = new unsigned char[wPar->width * wPar->height * 3]; // 3 bytes per pixel for RGB

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glPixelStorei(GL_PACK_ALIGNMENT, 1); // Set pixel alignment to 1 byte
    glReadPixels(0, 0, wPar->width, wPar->height, GL_RGB, GL_UNSIGNED_BYTE, pDat);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Create a temporary buffer to hold flipped image
    unsigned char* flippedData = new unsigned char[wPar->width * wPar->height * 3];

    // Flip the image vertically
    int rowSize = wPar->width * 3; // Size of each row in bytes
    for (int y = 0; y < wPar->height; ++y) {
        std::memcpy(flippedData + rowSize * y, pDat + rowSize * (wPar->height - 1 - y), rowSize);
    }

    // Save the flipped image
    dumpPPM(flippedData, wPar->width, wPar->height, fileName + ".ppm");

    // Free the image memory
    delete[] pDat;
    delete[] flippedData;
  }


  void Renderer::dumpPNG(unsigned char* data, int width, int height, std::string fileName)
  {
    //Follow the example from lodePNG, code taken from https://github.com/lvandeve/lodepng/blob/master/examples/example_encode.cpp
      
    //Encode the image
    unsigned error = lodepng::encode(fileName+".png", data, width, height);

    //if there's an error, display it
    if(error) std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
    
  }

  void Renderer::dumpPPM(unsigned char* data, int width, int height, std::string fileName)
  {
    FILE *fp = fopen(fileName.c_str(), "wb"); // 'b' for binary mode
    if (fp == nullptr) {
        std::cerr << "Error: Unable to open file " << fileName << std::endl;
        return;
    }
    fprintf(fp, "P6\n%d %d\n255\n", width, height);
    fwrite(data, 1, width * height * 3, fp); // 3 bytes per pixel for RGB
    fclose(fp);
  }


  //Input callback functions.
  static void callBackMouseClick(GLFWwindow* window, int button, int action, int mods)
  {
    if(button == GLFW_MOUSE_BUTTON_LEFT)
    {
      if(action == GLFW_PRESS)
      {
        mouseDown = true;
        resetCursorPos = true;
      }
      else
      {
        mouseDown = false;
      }
    }
    else if(button == GLFW_MOUSE_BUTTON_RIGHT)
    {
      if(action == GLFW_PRESS)
      {
        rightMouseDown = true;
        resetCursorPos = true;
      }
      else
      {
        rightMouseDown = false;
      }
    }
  }

  static void callBackMouseMotion(GLFWwindow* window, double xpos, double ypos)
  {
    if((mouseDown || rightMouseDown) && resetCursorPos)
    {
      oldX = xpos;
      oldY = ypos;
      resetCursorPos = false;
    }
    else if(mouseDown)
    {
      //std::cout << "Moving with mouse down!" << '\n';
      rotate_x += 0.002f*(xpos-oldX);
      rotate_y += 0.002f*(ypos-oldY);
      std::cout << "Updated rotate is now: " << rotate_x << " " << rotate_y << '\n';
      oldX = xpos;
      oldY = ypos;
    }
    else if(rightMouseDown)
    {
      float dist = (ypos-oldY);
      dist_z += dist*0.001f*dist_z;
      oldX = xpos;
      oldY = ypos;
    }
  }

  static void callBackKeyboard(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    //Change the value of the accelleration depending on what has been done.
    switch (key)
    {
      case GLFW_KEY_KP_4:
        if(simProp->a[0] == 0.0f)
        {
          simProp->a[0] = 0.01f*(simProp->a[1]+simProp->a[2]);
        }
        else
        {
          simProp->a[0] *= 1.05f;
        }

        break;
      case GLFW_KEY_KP_1:
        simProp->a[0] *= 0.95f;
        break;
      case GLFW_KEY_KP_5:
        if(simProp->a[1] == 0.0f)
        {
          simProp->a[1] = 0.01f*(simProp->a[0]+simProp->a[2]);
        }
        else
        {
          simProp->a[1] *= 1.05f;
        }
        break;
      case GLFW_KEY_KP_2:
        simProp->a[1] *= 0.95f;
        break;
      case GLFW_KEY_KP_6:
        if(simProp->a[2] == 0.0f)
        {
          simProp->a[1] = 0.01f*(simProp->a[0]+simProp->a[1]);
        }
        else
        {
          simProp->a[1] *= 1.05f;
        }
        break;
      case GLFW_KEY_KP_3:
        simProp->a[2] *= 0.95f;
        break;
      case GLFW_KEY_KP_ADD:
        simProp->renderInt *= 1.05;
        break;
      case GLFW_KEY_KP_MULTIPLY:
        simProp->visc *= 1.05;
        break;
      case GLFW_KEY_KP_DIVIDE:
        simProp->visc *= 0.95;
        break;
      case GLFW_KEY_KP_0:
        simProp->tDiff *= 1.05;
        break;
      case GLFW_KEY_KP_DECIMAL:
        simProp->tDiff *= 0.95;
        break;
      case GLFW_KEY_KP_SUBTRACT:
        simProp->renderInt *= 0.95;
        break;
      case GLFW_KEY_C:
        if(action == GLFW_PRESS)
          ChangeRenderOpt = true;
        break;
      case GLFW_KEY_S:
        if(action == GLFW_PRESS)
          simProp->saveFlag = true;
        break;
    }
  }

  static void callBackOnResize(GLFWwindow* window, int w, int h)
  {
    //Change the viewport
    glViewport(0, 0, w, h);

    
  }
}

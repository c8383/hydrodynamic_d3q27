/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef H_RENDEROBJECT
#define H_RENDEROBJECT

#include <GL/glew.h>
#include "ShaderProgram.h"
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

namespace Render
{

  class RenderObject
  {
  public:
    RenderObject(char *vertSource, char *fragSource, int numAttrib, int size, int nVals, char* type, char** attribNames, bool on = true, bool positional = true);
    ~RenderObject();

    bool on;
    bool positional;
    int numAttrib,size,nVals;
    GLint *attrib,posAttrib,mvpAttrib,strengthAttrib;
    GLuint vao,vbo;
    char *vertSource, *fragSource, *type, **attribNames;
    ShaderProgram* program;

    //Setting up the vao has to be done in conjunction with the renderer to get the correct geometry.
    bool bindVao();
    bool defineVao();
    bool enableAttributes();
    bool disableAttributes();
    float* getMappedPointer();
    void unMapPointer();

  private:
    struct cudaGraphicsResource *vbo_res;
    bool setUpShaders();
    bool setUpVbo();

  };

}

#endif
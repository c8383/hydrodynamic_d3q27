/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "RenderObject.h"

namespace Render
{

  RenderObject::RenderObject(char *vertSource, char *fragSource, int numAttrib, int size, int nVals, char *type, char** attribNames, bool on, bool positional)
  {
    this->vertSource = vertSource;
    this->fragSource = fragSource;
    this->numAttrib = numAttrib;
    this->size = size;
    this->nVals = nVals;
    this->type = type;
    this->attribNames = attribNames;
    this->on = on;
    this->positional = positional;

    //Set up the shaders.
    this->setUpShaders();

    //prepare the attrib pointers.
    attrib = new GLint[numAttrib];
    for(int i = 0; i < numAttrib; i++)
    {
      attrib[i] = program->getAttribLocation(this->attribNames[i]);
    }

    //Position and view matrix will always be called the same.
    if(positional)
    {
      posAttrib = program->getAttribLocation("pos");
    }

    mvpAttrib = program->getUniformLocation("MVP");
    strengthAttrib = program->getUniformLocation("strength");

    //Generate the vertex array to be used later.
    glGenVertexArrays(1, &vao);

    //Set up the vertex buffer object.
    this->setUpVbo();

  }

  RenderObject::~RenderObject()
  {
    delete[] this->vertSource;
    delete[] this->fragSource;
  }

  bool RenderObject::setUpShaders()
  {
    //Rely on command line callbacks if this fails.
    program = new ShaderProgram(this->vertSource, this->fragSource);

    //No failsafe for now.
    return true;
  }

  bool RenderObject::setUpVbo()
  {
    //This function sets up the vbo and the vbo_res for the cuda program.
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Given that the array size is correct.
    if(type[0] == 'f')
    {
      glBufferData(GL_ARRAY_BUFFER, size*nVals*sizeof(float), 0, GL_DYNAMIC_DRAW);
    }
    else if(type[0] == 'd')
    {
      glBufferData(GL_ARRAY_BUFFER, size*nVals*sizeof(double), 0, GL_DYNAMIC_DRAW);
    }
    else
    {
      glBufferData(GL_ARRAY_BUFFER, size*nVals*sizeof(char)*2, 0, GL_DYNAMIC_DRAW);
    }
    

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Make sure that the buffer is created and shared.
    cudaError_t ret = cudaGraphicsGLRegisterBuffer(&vbo_res, vbo, cudaGraphicsMapFlagsWriteDiscard);
    if(ret != cudaSuccess)
    {
      std::cout << "Failed to create the shared vbo instance!" << '\n';
      std::cout << "Error code: " << ret << '\n';

      //Failed to create instance, return false.
      return false;
    }

    //End of the line.
    return true;
  }

  bool RenderObject::bindVao()
  {
    //Create the vertex array object and bind the attrib locations to the buffers.
    glBindVertexArray(vao);

    return true;
  }

  bool RenderObject::defineVao()
  {
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    for(int i = 0; i < numAttrib; i++)
    {
      glEnableVertexAttribArray(attrib[i]);

      if(type[0] == 'f')
      {
        //size is assumed to be the size of the lattice.
        glVertexAttribPointer(attrib[i],1,GL_FLOAT,GL_FALSE,0,(void *)((size*i)*sizeof(float)));
      }
      else if(type[0] == 'd')
      {
        glVertexAttribPointer(attrib[i],1,GL_DOUBLE,GL_FALSE,0,(void *)((size*i)*sizeof(double)));
      }
      else
      {
        glVertexAttribPointer(attrib[i],1,GL_HALF_FLOAT,GL_FALSE,0,(void *)((size*i)*sizeof(char)*2));
      }
      
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return true;
  }

  bool RenderObject::enableAttributes()
  {
    if(positional)
    {
      glEnableVertexAttribArray(posAttrib);
    }

    for(int i = 0; i < numAttrib; i++)
    {
      glEnableVertexAttribArray(attrib[i]);
    }

    return true;
  }

  bool RenderObject::disableAttributes()
  {
    if(positional)
    {
      glDisableVertexAttribArray(posAttrib);
    }

    for(int i = 0; i < numAttrib; i++)
    {
      glDisableVertexAttribArray(attrib[i]);
    }

    return true;
  }

  float* RenderObject::getMappedPointer()
  {
    float* pointer;
    cudaError_t ret = cudaGraphicsMapResources(1, &vbo_res, 0);
    if(ret != cudaSuccess){std::cout << "FAILED AT cudaGraphicsMapResources!" << '\n'; return nullptr;}

    size_t nB;
    ret = cudaGraphicsResourceGetMappedPointer((void **)&pointer, &nB, vbo_res);
    if(ret != cudaSuccess) {std::cout << "FAILED AT cudaGraphicsResourceGetMappedPointer!" << '\n'; return nullptr;}

    return pointer;
  }

  void RenderObject::unMapPointer()
  {
    if(cudaGraphicsUnmapResources(1, &vbo_res, 0) != cudaSuccess)
    {
      std::cout << "FAILED TO UNMAP RESOURCE!" << '\n';
    }
  }
}

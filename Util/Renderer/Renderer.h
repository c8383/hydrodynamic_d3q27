/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef RENDERER_H
#define RENDERER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <iostream>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include "ShaderProgram.h"
#include "RenderObject.h"
#include <cmath>
#include <vector>
#include "../Simulation/SimulationProperties.h"
#include "../../Util/Renderer/lodepng.h"

namespace Render
{
  static bool mouseDown = false;
  static bool rightMouseDown = false;
  static bool resetCursorPos = false;
  static bool ChangeRenderOpt = false;
  static float rotate_x = 0.0f;
  static float rotate_y = 0.0f;
  static float oldX = 0;
  static float oldY = 0;
  static float dist_z = -75.0f;
  static Simulation::SimulationProperties *simProp;

  struct WindowParams
  {
    WindowParams(int width, int height, char* title);
    ~WindowParams();
    int width,height;
    char* title;
  };

  struct GeometryParams
  {
    GeometryParams(int Lx, int Ly, int Lz, float dx, float dy, float dz);
    ~GeometryParams();

    int Lx, Ly, Lz;
    float dx,dy,dz;
  };

  class Renderer
  {
  public:
    Renderer(WindowParams* wPar, GeometryParams* gPar, Simulation::SimulationProperties *simPropin,
                        char* type = "float");
    ~Renderer();

    /**
     * This function updates the latest data onto the glfw screen.
     */
    void view();


    /**
     * This function updates the latest data onto the glfw screen with predefined view properties.
     */
    void view(float rot_top, float rot_down, float dist, float scale);

    /**
     * This functions polls current events, not fully implemented yet.
     */
    void pollEvents();

    /**
     * This function adds anothjer renderobject.
     */
    void addRenderObject(RenderObject* renderObject);

    /**
     * This function dumps the screen contents 
     */
    void dumpScreen(std::string fileName);

    /**
     * Dumps a png with the given file-name.
     */
    void dumpPNG(unsigned char* data, int width, int height, std::string fileName);
    
    void dumpPPM(unsigned char* data, int width, int height, std::string fileName);


    //This is the VBO resource occupied by both the renderer and the solver, I.e.
    //the shared instance.
    struct cudaGraphicsResource *vbo_res;

  private:
    GLuint vboGeom;
    int arrSize;
    GLFWwindow* window;
    WindowParams* wPar;
    GeometryParams* gPar;

    //Transformation matrices and quaternion for rotation.
    glm::mat4 Projection;
    glm::mat4 View;
    glm::mat4 Model;
    glm::quat rot;

    //Vector of renderobjects, these needs to be held
    std::vector<RenderObject*> renderObjects;

    //The sources for the vertex and fragment shaders.
    char* type;

    //Function declarations.
    bool glfwSetup();
    bool GLSetup();
    bool geomSetUp();
  };

  //The callback functions, maybe this is bad program design?
  static void callBackMouseClick(GLFWwindow* window, int button, int action, int mods);
  static void callBackMouseMotion(GLFWwindow* window, double xpos, double ypos);
  static void callBackKeyboard(GLFWwindow* window, int key, int scancode, int action, int mods);
  static void callBackOnResize(GLFWwindow* window, int w, int h);

}

#endif
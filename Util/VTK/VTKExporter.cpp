/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "VTKExporter.h"



/***
 * Instantiates a VTKExporter object
 * @tparam T
 * @param size
 * @param dx
 * @param dy
 * @param dz
 * @param sX
 * @param sY
 * @param sZ
 */
template<typename T>
VTKExporter<T>::VTKExporter(int size, T dx, T dy, T dz, int sX, int sY, int sZ)
{
    //Instantiate all variables.
    this->dx = dx;
    this->dy = dy;
    this->dz = dz;

    this->sizeX = sX;
    this->sizeY = sY;
    this->sizeZ = sZ;

    this->size = size;

    vectors = vector<vectorSet<T>>();

    //A scalar is simply a vector-set with dimension 1, therefore we can do like this.
    scalars = vector<vectorSet<T>>();
}

/***
 * Deletes the VTKExporter objet.
 * @tparam T
 */
template<typename T>
VTKExporter<T>::~VTKExporter()
{
    //delete all the elements of the vectors, do not remove the begin-pointers
    //since they belong to other objects. Since no other pointers are present
    //it should delete everything.
    vectors.clear();
    scalars.clear();
}

/***
 * Exports all data to a file.
 * @tparam T
 * @param fileName
 * @return
 */
template<typename T>
bool VTKExporter<T>::printFile(string fileName)
{
    ofstream *vtkFile;

    try
    {
        //open an ofstream for file writing at the filename.
        vtkFile = new ofstream();
        vtkFile->open(fileName, ofstream::out | ofstream::trunc);

    }catch(int e)
    {
        cout << "Couldn't open file '" + fileName + "'";
        return false;
    }

    try
    {
        //Export the first two parts.
        this->prepareHeader(vtkFile);
        this->exportGeometry(vtkFile);

        //Export the vectors, TODO: Fix so this works for doubles too.
        if(vectors.size() != 0)
        {
          *vtkFile << "POINT_DATA " + to_string(size) << endl;
        }

        for (typename vector<vectorSet<T>>::iterator it = vectors.begin(); it != vectors.end(); ++it) {
            this->exportVector(vtkFile, *it);
        }

        //Export the scalars.
        if(scalars.size() != 0)
        {
          *vtkFile << "FIELD FieldData 1" << endl;
        }
        
        for (typename vector<vectorSet<T>>::iterator it = scalars.begin(); it != scalars.end(); ++it) {
            this->exportScalar(vtkFile, *it);
        }

    }
    catch(int e)
    {
        //Something failed in the code.
        vtkFile->close();
        delete vtkFile;
        return false;
    }

    vtkFile->close();
    delete vtkFile;

    return true;
}

template<typename T>
void VTKExporter<T>::prepareHeader(ofstream *file)
{
    //Write the header first.
    *file << "# vtk DataFile Version 2.0" << endl;
    *file << "Sample title" << endl;
    *file << "ASCII" << endl;
    *file << "DATASET STRUCTURED_GRID" << endl;
    *file << "DIMENSIONS " +  to_string(sizeX) + " " + to_string(sizeY) + " " + to_string(sizeZ) << endl;
}
/**
 *
 * @tparam T a double, float or any other floating point arithmetic with operations defined on it.
 * @param file A standard filestream object
 */
template<typename T>
void VTKExporter<T>::exportGeometry(ofstream *file)
{
    //Geometry always exported as float, this doesnt matter.
    *file << "POINTS " + to_string(size) + " float"<< endl;

    //Output the point positions
    for(int k = 0; k < sizeZ; k++)
    {
        for(int j = 0; j < sizeY; j++)
        {
            for(int i = 0; i < sizeX; i++)
            {
                *file << to_string(((float) i) * dx) + " " + to_string(((float) j) * dy) + " " + to_string(((float) k) * dz) << endl;
            }
        }
    }
}

template<typename T>
void VTKExporter<T>::exportVector(ofstream *file, vectorSet<T> vecSet)
{
    //First the preparation data.
    *file << "VECTORS " + vecSet.name + " " + vecSet.dataType << endl;

    //Now output all the data.
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < vecSet.nDim; j++)
        {
            //Because of how the struct is organized this will work.
            *file << to_string(vecSet.begin[i + j*size]) + " ";
        }

        *file << endl;
    }
}

template<typename T>
void VTKExporter<T>::exportScalar(ofstream *file, vectorSet<T> scalSet)
{
    //First the preparation data.
    *file << scalSet.name + " 1 " + to_string(size) + " " + scalSet.dataType << endl;

    //Now output all the data.
    for(int i = 0; i < size; i++)
    {
        //Because of how the struct is organized this will work.
        *file << to_string(scalSet.begin[i]) + " ";
        *file << endl;
    }
}

/***
 * Adds a scalar field with a typename and beginning.
 * @tparam T
 * @param dT
 * @param name
 * @param begin
 */
template<typename T>
void VTKExporter<T>::addScalar(string dT, string name, T *begin)
{
    scalars.push_back(vectorSet<T>(dT,name,1,begin));
}

/***
 * Adds a vector field with a typename and beginning.
 * @tparam T
 * @param dT
 * @param name
 * @param nDim
 * @param begin
 */
template<typename T>
void VTKExporter<T>::addVector(string dT, string name, int nDim, T *begin)
{
    vectors.push_back(vectorSet<T>(dT,name,nDim,begin));
}

/////Function declarations //////
template VTKExporter<float>::VTKExporter(int size, float dx, float dy, float dz, int sX, int sY, int sZ);
template VTKExporter<float>::~VTKExporter();

template bool VTKExporter<float>::printFile(string fileName);
template void VTKExporter<float>::addScalar(string dT, string name, float* begin);
template void VTKExporter<float>::addVector(string dT, string name, int nDim, float* begin);

template void VTKExporter<float>::prepareHeader(ofstream *file);
template void VTKExporter<float>::exportGeometry(ofstream *file);
template void VTKExporter<float>::exportVector(ofstream *file, vectorSet<float> vecSet);
template void VTKExporter<float>::exportScalar(ofstream *file, vectorSet<float> scalSet);

template VTKExporter<double>::VTKExporter(int size, double dx, double dy, double dz, int sX, int sY, int sZ);
template VTKExporter<double>::~VTKExporter();

template bool VTKExporter<double>::printFile(string fileName);
template void VTKExporter<double>::addScalar(string dT, string name, double* begin);
template void VTKExporter<double>::addVector(string dT, string name, int nDim, double* begin);

template void VTKExporter<double>::prepareHeader(ofstream *file);
template void VTKExporter<double>::exportGeometry(ofstream *file);
template void VTKExporter<double>::exportVector(ofstream *file, vectorSet<double> vecSet);
template void VTKExporter<double>::exportScalar(ofstream *file, vectorSet<double> scalSet);

/*
template VTKExporter<__half>::VTKExporter(int size, __half dx, __half dy, __half dz, int sX, int sY, int sZ);
template VTKExporter<__half>::~VTKExporter();

template bool VTKExporter<__half>::printFile(string fileName);
template void VTKExporter<__half>::addScalar(string dT, string name, __half* begin);
template void VTKExporter<__half>::addVector(string dT, string name, int nDim, __half* begin);

template void VTKExporter<__half>::prepareHeader(ofstream *file);
template void VTKExporter<__half>::exportGeometry(ofstream *file);
template void VTKExporter<__half>::exportVector(ofstream *file, vectorSet<__half> vecSet);
template void VTKExporter<__half>::exportScalar(ofstream *file, vectorSet<__half> scalSet);*/

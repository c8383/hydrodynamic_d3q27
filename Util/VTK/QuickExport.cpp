/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "QuickExport.h"

template<typename T>
void exportNow(int size, int dim, std::string name, T* vals)
{
  std::ofstream *file;

  try
  {
      //open an ofstream for file writing at the filename.
      file = new std::ofstream();
      file->open(name, std::ofstream::out | std::ofstream::trunc);

  }catch(int e)
  {
      std::cout << "Couldn't open file: '" + name + "'";
  }

  try
  {
    //Loop through all values and export them.
    //Now output all the data.
    for(uint i = 0; i < size; i++)
    {
        for(uint j = 0; j < dim; j++)
        {
            //Because of how the struct is organized this will work.
            *file << std::to_string(vals[i + j*size]) + " ";
        }

        *file << std::endl;
    }

  }
  catch(int e)
  {
      //Something failed in the code.
      file->close();
      delete file;
  }

  file->close();
  delete file;

}


template<typename T>
void exportBinary(int size, std::string name, T* vals, int dim)
{
    //Adapt to paraviews standard.
    if(dim < 2)
    {
        std::ofstream fout;
        fout.open(name, std::ios::binary | std::ios::out);
        fout.write((char*) vals, size*sizeof(T));
        fout.close();
    }
    else
    {
        T* lVals = new T[size];

        for(uint i = 0; i < size/dim; i++)
        {   
            for(uint j = 0; j < dim; j++)
            {
                lVals[i*dim+j] = vals[i+j*size/dim];
            }
        }

        std::ofstream fout;
        fout.open(name, std::ios::binary | std::ios::out);
        fout.write((char*) lVals, size*sizeof(T));
        fout.close();

        //Free the memory.
        delete[] lVals;
    }

}


template void exportNow<float>(int size, int dim, std::string name, float* vals);
template void exportNow<double>(int size, int dim, std::string name, double* vals);

template void exportBinary<float>(int size, std::string name, float* vals, int dim);
template void exportBinary<double>(int size, std::string name, double* vals, int dim);

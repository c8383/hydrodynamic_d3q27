/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef VTKEXPORTER_VTKEXPORTER_H
#define VTKEXPORTER_VTKEXPORTER_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cuda_fp16.h>

 using namespace std;

 template <typename D>
 struct vectorSet
 {
     vectorSet(string dT, string name, int nDim, D* begin)
     {
         this->dataType = dT;
         this->name = name;
         this->nDim = nDim;
         this->begin = begin;
     }

     string dataType;
     string name;
     int nDim;
     D* begin;
 };


 template <typename T>
 class VTKExporter
 {

 public:
     VTKExporter(int size, T dx, T dy, T dz, int sX, int sY, int sZ);
     ~VTKExporter();

     //All the data pointers are already passed in the add-scalars step and so on.
     bool printFile(string fileName);

     void addScalar(string dT, string name, T* begin);
     void addVector(string dT, string name, int nDim, T* begin);

 private:
     void prepareHeader(ofstream *file);
     void exportGeometry(ofstream *file);
     void exportVector(ofstream *file, vectorSet<T> vecSet);
     void exportScalar(ofstream *file, vectorSet<T> scalSet);

     int size;
     int sizeX,sizeY,sizeZ;
     T dx,dy,dz;

     vector<vectorSet<T>> vectors;
     vector<vectorSet<T>> scalars;

 };


 #endif //VTKEXPORTER_VTKEXPORTER_H

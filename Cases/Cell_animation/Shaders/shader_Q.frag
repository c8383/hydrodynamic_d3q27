#version 460
out vec4 LFragment;

in vec4 colorParticle;

void main()
{
  //Just a straightforward coloring for now.
  LFragment = vec4( colorParticle.x, colorParticle.y, colorParticle.z, colorParticle.w );
}

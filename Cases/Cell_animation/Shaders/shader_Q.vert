#version 460

in vec3 pos;
in float Q;
uniform mat4 MVP;
uniform float strength;

#define maxVal 1.0f
#define minVal 0.0f

out vec4 colorParticle;

void main()
{
  gl_Position = MVP*(vec4( pos.x, pos.y, pos.z, 1 ));
  //colorParticle = strength*vec4(sin((Q-minVal)*3*3.14f)*0.01,sin((Q-minVal)*3*3.14f-3.14/3.0f)*0.01,sin((Q-minVal)*3*3.14f-2.0f*3.14/3.0f)*0.01,0.1f);
  //colorParticle = strength*vec4(0.01f*exp(-(Q)*(Q)),0.01f*exp(-(Q-0.5)*(Q-0.5)),0.01f*exp(-(Q-1.0)*(Q-1.0)),0.1f);
  //colorParticle = strength*vec4(exp(-pow(4.0*T-4.0,2.0)),exp(-pow(4.0*T-2.0,2.0)),exp(-pow(4.0*T,2.0)),0.1f);
  colorParticle = vec4(exp(-pow(4.0*Q*strength-4.0,2.0f)),exp(-pow(4.0*Q*strength-3.0,2.0f)),exp(-pow(4.0*Q*strength-2.0,2.0f)),0.0125f);
}

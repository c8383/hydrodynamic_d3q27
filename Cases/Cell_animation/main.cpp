/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <iostream>
#include <ctime>
#include <cmath>
#include <fstream>
#include <string>

#include "../../Util/VTK/VTKExporter.h"
#include "../../Util/Renderer/Renderer.h"
#include "../../Kernels/Utils/D3/sumvel.h"
#include "../../Util/Renderer/RenderObject.h"
#include "../../Util/Simulation/SimulationProperties.h"
#include "../../Util/Simulation/Dataexporter.h"
#include "../../Util/Simulation/Logger.h"
#include "../../Util/VTK/QuickExport.h"

#include <cuda_runtime.h>

#include "calc.h"

//This is the floating point precision definition, use T wherever you would use float or double.
#define T float

bool checkInRectangle(int posXstart, int posYstart, int sizeX, int sizeY, int x, int y)
{
  if( x > posXstart &&
      x < posXstart + sizeX && 
      y > posYstart &&
      y < posYstart + sizeY)
  {
    return true;
  }
  else
  {
    return false;
  }
}


void getError()
{
  cudaError err = cudaGetLastError();
  if(cudaSuccess != err)
  {
    std::cout << "An error occured in the kernel..." << '\n';
    std::cout << cudaGetErrorString( err ) << '\n';
  }
}

int main(int argc, char** arg)
{

  //I think that this whole first part should be transformed into a simulation handler.
  //it should be handled automatically.
  uint Lx;
  uint Ly;
  uint Lz;
  uint Lc;

  int gpu_index;

  int nVisc;
  int* initSweeps;
  int* nSweeps;
  int cellType;
  int renderActive;
  int screenSize;
  int imageIncrement;
  float *visc;
  float *inputVel;
  float rC;
  float phi;


  if(argc < 7)
  {
    std::cout << "Wrong amount of input arguments!" << std::endl;
    return 0;
  }
  else
  {

    Lc = atoi(arg[1]);
    rC = atof(arg[2]); //defined in terms of Lc.
    cellType = atoi(arg[3]);
    nVisc = atoi(arg[4]);
    phi = atof(arg[5]);
    renderActive = atoi(arg[6]); //Enable an option to disable rendering entirely.
    gpu_index = atoi(arg[7]);
    screenSize = atoi(arg[8]);
    imageIncrement = atoi(arg[9]);

    visc = new float[nVisc];
    nSweeps = new int[nVisc];
    initSweeps = new int[nVisc];
    inputVel = new float[nVisc];

    for(int i = 0; i < nVisc; i++)
    {
      visc[i] = atof(arg[10+i]);
      nSweeps[i] = atoi(arg[10+nVisc+i]);
      initSweeps[i] = atoi(arg[10+2*nVisc+i]);
      inputVel[i] = atof(arg[10+3*nVisc+i]);
    }

  }

  //Set the cuda device to the chosen one.
  cudaSetDevice(gpu_index);

  Lx = Lc*12;
  Ly = Lc*6;
  Lz = Lc;

  //Here L3 will become larger for reasons earlier discussed.
  uint L3 = (Lx+2)*(Ly+2)*(Lz+2);
  uint valOn = L3;


  //Distribution functions and velocity.
  T* f1 = new T[L3*27];
  T* f2 = new T[L3*27];
  T* u = new T[L3*3];
  T* rho = new T[L3];
  T* rho_avg = new T[L3];
  T* u_avg = new T[L3*3];
  T* u_std = new T[L3*3];
  T* f1_d, *f2_d, *u_d, *Q_d, *Rho_d,*u_d_avg, *rho_d_avg, *u_d_std;
  T* ux_plane = new T[(Ly+2)*(Lz+2)]; for(int i = 0; i < (Ly+2)*(Lz+2); i++) {ux_plane[i] = 0.0f;} //Zero array on creation.
  //T phi = 0.63268f; //This factor should be calculated in some way instead.

  //Wall values as well.
  char* wall = new char[L3];
  char* wall_d;

  //The sum array, length is ceiling of integer-division of block length and blabla.
  int arrLength = (L3)/sumBlockSize + (L3 % sumBlockSize != 0);
  std::cout << "Sum array length is: " << arrLength << '\n';
  T* sumArray = new T[arrLength];
  T* sumArray_d;

  cudaMalloc(&f1_d, L3*27*sizeof(T));
  getError();
  cudaMalloc(&f2_d, L3*27*sizeof(T));
  getError();
  cudaMalloc(&wall_d, L3*sizeof(char));
  getError();
  cudaMalloc(&sumArray_d, arrLength*sizeof(T));
  getError();
  cudaMalloc(&u_d_avg, L3*3*sizeof(T));
  getError();
  cudaMalloc(&rho_d_avg, L3*sizeof(T));
  getError();
  cudaMalloc(&u_d_std, L3*3*sizeof(T));
  getError();

  cudaDeviceSynchronize();

  //there are 27 different weigths.
  float w[]= {0.004629629629629629f,
0.018518518518518517f,
0.004629629629629629f,
0.018518518518518517f,
0.07407407407407407f,
0.018518518518518517f,
0.004629629629629629f,
0.018518518518518517f,
0.004629629629629629f,
0.018518518518518517f,
0.07407407407407407f,
0.018518518518518517f,
0.07407407407407407f,
0.2962962962962963f,
0.07407407407407407f,
0.018518518518518517f,
0.07407407407407407f,
0.018518518518518517f,
0.004629629629629629f,
0.018518518518518517f,
0.004629629629629629f,
0.018518518518518517f,
0.07407407407407407f,
0.018518518518518517f,
0.004629629629629629f,
0.018518518518518517f,
0.004629629629629629f};

  for (int i = 0; i < L3; i++)
  {
    for(int m = 0; m < 27; m++)
    {

      f1[i+m*L3] = w[m];
      f2[i+m*L3] = w[m];
    }

    float rad2 = (rC*((float)Lc)-0.5f)*(rC*((float)Lc)-0.5f);
    int porousCutoffMax = (Lx-Lc);
    int porousCutOffMin = Lc;
    int cellSize = Lc;

    if(cellType == 0)
    {
      int spacy = Lc;
      int spacx = 2*Lc;

      float rcX_1 = ((float)(i%(Lx+2)%spacx)-((float)spacx)/2.0f-((float)cellSize)/2.0f-0.5f);
      float rcY_1 = ((float)(((i/(Lx+2))%(Ly+2))%spacy)-((float)spacy)/2.0f-0.5f);
      float rcX_2 = ((float)(i%(Lx+2)%spacx)-((float)spacx)/2.0f+((float)cellSize)/2.0f-0.5f);
      float rcY_2 = ((float)(((i/(Lx+2))%(Ly+2)-cellSize/2)%spacy)-((float)spacy)/2.0f-0.5f);

      //Make this into a function where you can choose the shape.
      if((rcX_1*rcX_1+rcY_1*rcY_1 <= rad2 && i%(Lx+2) > cellSize && i%(Lx+2) < porousCutoffMax) || //First set of cylinders
            (rcX_2*rcX_2 + rcY_2*rcY_2 <= rad2 && //Second set of cylinders.
            i%(Lx+2) > cellSize && i%(Lx+2) < porousCutoffMax && (i/(Lx+2))%(Ly+2) > cellSize/2 && (i/(Lx+2))%(Ly+2) < (Ly+2)-cellSize/2) || 
            i/((Lx+2)*(Ly+2)) == 0 || i/((Lx+2)*(Ly+2)) == Lz+1 || (i/(Lx+2))%(Ly+2) == 0 || (i/(Lx+2))%(Ly+2) == Ly+1)
      {
        wall[i] = 'w';
        valOn--;
      }
      else
      {
        wall[i] = '0';
      }
    }
    else
    {
      int spacy = Lc;
      int spacx = Lc;

      //Make this into a function where you can choose the shape.
      if((((i%(Lx+2))%spacx-spacx/2)*((i%(Lx+2))%spacx-spacx/2) + (((i/(Lx+2))%(Ly+2))%spacy-spacy/2)*(((i/(Lx+2))%(Ly+2))%spacy-spacy/2) <= rad2 && i%(Lx+2) > cellSize && i%(Lx+2) < porousCutoffMax))// || 
      //((i%(Lx+2)) >= porousCutoffMax && (i%(Lx+2)) <= porousCutoffMax+cellSize && ((i/(Lx+2))%(Ly+2)) < cellSize && ((i%(Lx+2))%spacx-spacx/2)*((i%(Lx+2))%spacx-spacx/2) + (((i/(Lx+2))%(Ly+2))%spacy-spacy/2)*(((i/(Lx+2))%(Ly+2))%spacy-spacy/2) <= rad2 ))
      {
        wall[i] = 'w';
        valOn--;
      }
      else
      {
        wall[i] = '0';
      }
    }

    u_avg[i] = 0.0f;
    u_avg[i+L3] = 0.0f;
    u_avg[i+2*L3] = 0.0f;

    u_std[i] = 0.0f;
    u_std[i+L3] = 0.0f;
    u_std[i+2*L3] = 0.0f;

    rho_avg[i] = 0.0f;
  }

  //Dump all the wall data.
  char* dWall = new char[L3];

  //Set neighbour elements.
  for(int i = 0; i < L3; i++)
  {
    if(wall[i] == 'w')
    {

      int xPos = (i+Lx+2)%(Lx+2);
      int yPos = (i/(Lx+2)+(Ly+2))%(Ly+2);
      int zPos = (i/((Lx+2)*(Ly+2))+(Lz+2))%(Lz+2);

      for(int k = 0; k < 27; k++)
      {
        dWall[(Lx+2+xPos+((k%3)-1))%(Lx+2)+((yPos+Ly+2+((k/3)%3)-1)%(Ly+2))*(Lx+2)+((zPos+Lz+2+(k/9)-1)%(Lz+2))*(Lx+2)*(Ly+2)] = 2;
      }
    }
  }

  for(int i = 0; i < L3; i++)
  {
    if(wall[i] == 'w')
    {
      dWall[i] = 0;
    }
    else if(dWall[i] != 2)
    {
      dWall[i] = 1;
    }
  }

  std::ofstream fout;
  fout.open("wall.raw", std::ios::binary | std::ios::out);
  fout.write(dWall, L3*sizeof(char));
  fout.close();

  //delete[] dWall;

  std::cout << "Cells that are on: " << valOn << '\n';


  //Initialize the renderer with the acceleration.
  Simulation::SimulationProperties *simProp = new Simulation::SimulationProperties();
  simProp->a = new float[3];
  simProp->a[0] = 0.000005f;
  simProp->a[1] = 0.00000000f;
  simProp->a[2] = 0.00000000f;
  simProp->renderInt = 2.0f;
  simProp->visc = 0.004f;
  Render::Renderer* render;

  Render::RenderObject *Q_renderObj;
  Render::RenderObject *Rho_renderObj;
  Render::RenderObject *U_renderObj;


  if(renderActive)
  {
    render = new Render::Renderer(new Render::WindowParams(screenSize,screenSize,"LBM-Window"),
                                                new Render::GeometryParams(Lx+2,Ly+2,Lz+2,1.0f,1.0f,1.0f),
                                                simProp);

    //Add the renderobjects to the renderer.
    char* qRenderString[1] = {"Q"};
    Q_renderObj = new Render::RenderObject("Shaders/shader_Q.vert",
                                                          "Shaders/shader_Q.frag",
                                                          1, L3, 1, "float",
                                                          qRenderString, true);

    render->addRenderObject(Q_renderObj);

    //Add a density renderobject as well.
    char* rhoRenderString[1] = {"Rho"};
    Rho_renderObj = new Render::RenderObject("Shaders/shader_rho.vert",
                                                          "Shaders/shader_rho.frag",
                                                          1, L3, 1, "float",
                                                          rhoRenderString, false);

    render->addRenderObject(Rho_renderObj);


    char* uRenderString[3] = {"u","v","w"};
    U_renderObj = new Render::RenderObject("Shaders/shader.vert",
                                                          "Shaders/shader.frag",
                                                          3, L3, 3, "float",
                                                          uRenderString, false);

    render->addRenderObject(U_renderObj);
  }
  else
  {
    //Make sure that the buffers exist.
    cudaMalloc(&u_d, L3*3*sizeof(T));
    getError();
    cudaMalloc(&Rho_d, L3*sizeof(T));
    getError();
    cudaMalloc(&Q_d, L3*sizeof(T));
    getError();
  }

  cudaMemcpy(f1_d, f1, L3*27*sizeof(T), cudaMemcpyHostToDevice);
  cudaMemcpy(f2_d, f2, L3*27*sizeof(T), cudaMemcpyHostToDevice);
  cudaMemcpy(wall_d, wall, L3*sizeof(char), cudaMemcpyHostToDevice);
  cudaMemcpy(sumArray_d, sumArray, arrLength*sizeof(T), cudaMemcpyHostToDevice);
  cudaMemcpy(u_d_avg, u_avg, L3*3*sizeof(T), cudaMemcpyHostToDevice); getError();
  cudaMemcpy(u_d_std, u_std, L3*3*sizeof(T), cudaMemcpyHostToDevice); getError();
  cudaDeviceSynchronize();

  //Free memory from ram immediately, it is no longer necessary.
  delete[] f1;
  delete[] f2;


  //Perform the algorithm copying the velocity data evey N-times.
  T vel = 1e-16;
  T velKoch = 0.0f;
  T oldVel = 0.0f;

  //The block and grid remain the same size but the position is shifted in the solver.
  dim3 block(Lx/4,1,1);
  dim3 grid(4,Ly,Lz);

  //This update should only be applied to
  dim3 blockInlet(Ly+2,1,1);
  dim3 gridInlet(1,Lz+2,1);

  dim3 blockWallZ((Lx+2)/4,1,1);
  dim3 gridWallZ(4,Ly+2,1);

  dim3 blockWallY((Lx+2)/4,1,1);
  dim3 gridWallY(4,Lz+2,1);

  vel = 1e-5;
  oldVel = 0.0f;

  int nAvg = 0;

  T accx = (T)simProp->a[0];

  clock_t time_old = clock();

  //Create the logger.
  Logging::Logger *logHandle = new Logging::Logger("logfile.log");

  int Lx2 = Lx+2;
  int Ly2 = Ly+2;
  int Lz2 = Lz+2;
  int Lxy2 = Lx2*Ly2;
  int velOff = 0;
  int velOff2 = Lx+1;

  //The PID regulator for the inlet velocity.
  float targetVel = 0.01f;
  float error = 0.0f; float pConst = 0.1f;
  float lastError = 0.0f; float dConst = 0.01f;
  float cumError = 0.0f; float sConst = 0.001f;
  float corr = 0.0f;

  int timeIndex = 0;
  int timeIndex_ux = 0;

	float corrVel = inputVel[0];

  //Actually the velocity only needs to be set once (for this cond at least).
  queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff, Lx2, Lxy2, corrVel, f1_d, wall_d); getError();
  queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff2, Lx2, Lxy2, corrVel, f1_d, wall_d); getError();
  queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff, Lx2, Lxy2, corrVel, f2_d, wall_d); getError();
  queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff2, Lx2, Lxy2, corrVel, f2_d, wall_d); getError();

  Q_d = (T*)Q_renderObj->getMappedPointer(); //This cast is not entirely legit
  u_d = (T*)U_renderObj->getMappedPointer();
  Rho_d = (T*)Rho_renderObj->getMappedPointer();

  for(int k = 0; k < nVisc; k++)
  {
    //The render intensity should scale by the current darcy velocity.
    simProp->renderInt = 2.0f/inputVel[k];

    //Run initialization sweep.
    for(int i = 0; i < initSweeps[k]; i++)
    {
      if(i%(10) == 0)
      {
        //Get the velocity pointer from the render instance.
        if(renderActive)
        {
          ///Q_d = (T*)Q_renderObj->getMappedPointer(); //This cast is not entirely legit
          ///u_d = (T*)U_renderObj->getMappedPointer();
          ///Rho_d = (T*)Rho_renderObj->getMappedPointer();
        }

        //This should all be controlled by a separate function.
        queueCollideAndStream<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f1_d, f2_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();

        
        queueCollideAndStreamURhoQ<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f2_d, f1_d, u_d, Rho_d, Q_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();

      }
      else
      {
        queueCollideAndStream<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f1_d, f2_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        
        queueCollideAndStream<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f2_d, f1_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
      }

      //Update the velocity check.
      if(i%10 == 0)
      { 
        if(i%100 == 0)
        {
          cudaDeviceSynchronize();
          queueSumVelU<T>(arrLength, u_d, sumArray_d, L3); getError();
          cudaDeviceSynchronize();
          
          cudaMemcpy(sumArray, sumArray_d, arrLength*sizeof(T), cudaMemcpyDeviceToHost);
          std::cout << "It took: " << double(- time_old + clock()) / CLOCKS_PER_SEC << " --> ";
          time_old = clock();
          oldVel = vel;
          vel = 0.0f;

          for(int ii = 0; ii < arrLength; ii++)
          {
            vel += sumArray[ii];
          }

          //This is the darcy velocity.
          velKoch = vel/((T) L3);
          vel = vel/((T)Lx*Ly*Lz);

          error = inputVel[k]-vel;
          cumError += error;

          //Adjust the values on inlet and outlet.
          corrVel += error*pConst;

          lastError = error;

          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff, Lx2, Lxy2, corrVel, f1_d, wall_d); getError();
          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff2, Lx2, Lxy2, corrVel, f1_d, wall_d); getError();
          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff, Lx2, Lxy2, corrVel, f2_d, wall_d); getError();
          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff2, Lx2, Lxy2, corrVel, f2_d, wall_d); getError();

          std::cout << "Current U: " << vel << std::endl;
          std::cout << "Current U_int: " << vel/(phi) << " Visc is: " << visc[k] << std::endl;
          std::cout << "Reynolds number is: " << (vel/(phi))*((float)Lc)*2.0f*rC/(visc[k]) << std::endl;
        }

        if(i%100 == 0)
        {
          cudaDeviceSynchronize();
          //Transfer x-direction velocity data and dump.
          //Just copy all the velocities from the GPU and save what is intresting, stupid brute-force approach but will probably work...
 
 
          if(cudaMemcpy(u, u_d, L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess) //Only care about copying the x-values.
          {
            std::cout << "Failed to copy memory u_d, error: " << '\n';
            getError();
          }
          cudaDeviceSynchronize();
 
          for(int loci = 0; loci < (Lz+2)*(Ly+2); loci++)
          {
            ux_plane[loci] = u[loci*(Lx+2)+Lc*5];
          }
 
          //Dump the ux-plane as a binary file.
          exportBinary<T>((Ly+2)*(Lz+2),"data/centVel_" + std::to_string(timeIndex_ux) + ".raw",ux_plane,1);
 
          timeIndex_ux++;
        }


        //Try drawing images and dump.
        if(timeIndex%imageIncrement==0 && renderActive)
        {

          Rho_renderObj->unMapPointer();
          Q_renderObj->unMapPointer();
          U_renderObj->unMapPointer();


          //Very hacky, please dont do this.
          glPointSize(1.5f);
          render->view(3.14f/4.0f,0.8f,5000.0f/6.0f,(float)(20.0*((float)Lc)/(2.0f*1.414f)));
          std::string indexPPM = std::to_string(timeIndex/imageIncrement);
          indexPPM.insert(indexPPM.begin(), 10-indexPPM.length(),'0');
          std::string fileName = "figures/" + std::to_string(k) + "/dataIso_"+indexPPM;
          render->dumpScreen(fileName);

          glPointSize(1.0f);
          render->view(0.0f,0.0f,5000.0f/8.0f,(float)(20.0*((float)Lc)/2));
          fileName = "figures/" + std::to_string(k) + "/dataTop_"+indexPPM;
          render->dumpScreen(fileName);


          Q_d = (T*)Q_renderObj->getMappedPointer(); //This cast is not entirely legit
          u_d = (T*)U_renderObj->getMappedPointer();
          Rho_d = (T*)Rho_renderObj->getMappedPointer();
        }

      }

      timeIndex++;
    }

    //Specify all the points the should be monitored and place them in a vector.
    std::vector<Simulation::Dataexporter<T>*> exportPoints;

    //Add all the vectors to it and push back, index in the same way as for the LDA-Data. These points should be specifiable in an external file.
    if(cellType == 0)
    {
      for(int i = 0; i < 65; i++)
      {
        if(i < 25)
        {
          exportPoints.push_back(new Simulation::Dataexporter<T>(Lx, Ly, Lz,
                                                                  ((Lc+Lc/2+1)+(i/5)*Lc*2)+((Lx+2)*(1+Lc+Lc*(i%5)))+L3/2, 3, L3,
                                                                  10, 10, nSweeps[k]/10, &u_d, "data/centerPoint_" + std::to_string(i) + "_",i));
        }
        else if(i < 45)
        {
          exportPoints.push_back(new Simulation::Dataexporter<T>(Lx, Ly, Lz,
                                                                  ((2*Lc+Lc/2+1)+((i-25)/4)*Lc*2)+((Lx+2)*(1+Lc+Lc/2+Lc*((i-25)%4)))+L3/2, 3, L3,
                                                                  10, 10, nSweeps[k]/10, &u_d, "data/centerPoint_" + std::to_string(i) + "_",i));
        }
        else if(i < 55)
        {
          exportPoints.push_back(new Simulation::Dataexporter<T>(Lx, Ly, Lz,
                                                                  ((Lc+Lc/2+1)+((i-45)/2)*Lc*2)+(Lx+2)*(1+(Lc/2-(int)(rC*((float)Lc)))/2+((i-45)%2)*(Lc*6-(Lc/2-(int)(rC*((float)Lc)))))+L3/2, 3, L3,
                                                                  10, 10, nSweeps[k]/10, &u_d, "data/centerPoint_" + std::to_string(i) + "_",i));
        }
        else
        {
          exportPoints.push_back(new Simulation::Dataexporter<T>(Lx, Ly, Lz,
                                                                  ((2*Lc+Lc/2+1)+((i-55)/4)*Lc*2)+(Lx+2)*(1+(Lc-(int)(rC*((float)Lc)))/2+((i-55)%2)*(Lc*6-(Lc-(int)(rC*((float)Lc)))))+L3/2, 3, L3,
                                                                  10, 10, nSweeps[k]/10, &u_d, "data/centerPoint_" + std::to_string(i) + "_",i));

        }

      }

    }
    else
    {
      for(int i = 0; i < 45; i++)
      {
          exportPoints.push_back(new Simulation::Dataexporter<T>(Lx, Ly, Lz,
                                                                  ((Lc+Lc/2+1)+(i/5)*Lc)+((Lx+2)*(1+Lc+Lc*(i%5)))+L3/2, 3, L3,
                                                                  10, 10, nSweeps[k]/10, &u_d, "data/centerPoint_" + std::to_string(i) + "_",i));
      }
    }

    //Change the names of the dataexporters to match the viscosity.
    for(auto x : exportPoints)
    {
      x->changeTitle("data/porePoint_" + std::to_string(x->index) + "_" + std::to_string(k) + ".pdat");
    }

    for(int i = 0; i < nSweeps[k]; i++)
    {
      if(i%(10) == 0)
      {
        //Get the velocity pointer from the render instance.
        if(renderActive)
        {
          //Q_d = (T*)Q_renderObj->getMappedPointer(); //This cast is not entirely legit
          //u_d = (T*)U_renderObj->getMappedPointer();
          //Rho_d = (T*)Rho_renderObj->getMappedPointer();
        }

        //This should all be controlled by a separate function.
        queueCollideAndStream<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f1_d, f2_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();

        
        queueCollideAndStreamURhoQ<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f2_d, f1_d, u_d, Rho_d, Q_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();

      }
      else
      {
        queueCollideAndStream<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f1_d, f2_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f1_d, f2_d); getError();
        
        queueCollideAndStream<T>(&block, &grid, L3, (uint)Lxy2, (uint)Lx2, (uint)Ly2, (uint)Lz2, (uint)1, (uint)1, (uint)1, 0.0f, 0.0f, 0.0f, 1.0f/(3.0f*visc[k]+0.5f), f2_d, f1_d, wall_d); getError();
        queueWallZP<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallZM<T>(&blockWallZ, &gridWallZ, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYP<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
        queueWallYM<T>(&blockWallY, &gridWallY, L3, Lx2, Ly2, Lz2, f2_d, f1_d); getError();
      }

      //Update the velocity check.
      if(i%10 == 0)
      { 
        if(i%100 == 0)
        {
          cudaDeviceSynchronize();
          queueSumVelU<T>(arrLength, u_d, sumArray_d, L3); getError();
          cudaDeviceSynchronize();
          cudaMemcpy(sumArray, sumArray_d, arrLength*sizeof(T), cudaMemcpyDeviceToHost);
          std::cout << "It took: " << double(- time_old + clock()) / CLOCKS_PER_SEC << " --> ";
          time_old = clock();
          oldVel = vel;
          vel = 0.0f;

          for(int ii = 0; ii < arrLength; ii++)
          {
            vel += sumArray[ii];
          }

          //This is the darcy velocity.
          velKoch = vel/((T) L3);
          vel = vel/((T)Lx*Ly*Lz);

          error = inputVel[k]-vel;
          cumError += error;

          //Adjust the values on inlet and outlet.
          corrVel += error*pConst;

          lastError = error;

          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff, Lx2, Lxy2, corrVel, f1_d, wall_d); getError();
          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff2, Lx2, Lxy2, corrVel, f1_d, wall_d); getError();
          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff, Lx2, Lxy2, corrVel, f2_d, wall_d); getError();
          queueSetVel<T>(&blockInlet, &gridInlet, L3, velOff2, Lx2, Lxy2, corrVel, f2_d, wall_d); getError();

          std::cout << "Current U: " << vel << std::endl;
          std::cout << "Current U_int: " << vel/(phi) << " Visc is: " << visc[k] << std::endl;
          std::cout << "Reynolds number is: " << (vel/(phi))*((float)Lc)*2.0f*rC/(visc[k]) << std::endl;
        }

        //Central plane animation.
        if(i%100 == 0)
        {
          cudaDeviceSynchronize();
          //Transfer x-direction velocity data and dump.
          //Just copy all the velocities from the GPU and save what is intresting, stupid brute-force approach but will probably work...
 
 
          if(cudaMemcpy(u, u_d, L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess) //Only care about copying the x-values.
          {
            std::cout << "Failed to copy memory u_d, error: " << '\n';
            getError();
          }
          cudaDeviceSynchronize();
 
          for(int loci = 0; loci < (Lz+2)*(Ly+2); loci++)
          {
            ux_plane[loci] = u[loci*(Lx+2)+Lc*5];
          }
 
          //Dump the ux-plane as a binary file.
          exportBinary<T>((Ly+2)*(Lz+2),"data/centVel_" + std::to_string(timeIndex_ux) + ".raw",ux_plane,1);
 
          timeIndex_ux++;
        }

        //Try drawing images and dump.
        if(timeIndex%imageIncrement==0 && renderActive)
        {

          Rho_renderObj->unMapPointer();
          Q_renderObj->unMapPointer();
          U_renderObj->unMapPointer();


          //Very hacky, please dont do this.
          glPointSize(1.5f);
          render->view(3.14f/4.0f,0.8f,5000.0f/6.0f,(float)(20.0*((float)Lc)/(2.0f*1.414f)));
          std::string indexPPM = std::to_string(timeIndex/imageIncrement);
          indexPPM.insert(indexPPM.begin(), 10-indexPPM.length(),'0');
          std::string fileName = "figures/" + std::to_string(k) + "/dataIso_"+indexPPM;
          render->dumpScreen(fileName);

          glPointSize(1.0f);
          render->view(0.0f,0.0f,5000.0f/8.0f,(float)(20.0*((float)Lc)/2));
          fileName = "figures/" + std::to_string(k) + "/dataTop_"+indexPPM;
          render->dumpScreen(fileName);


          Q_d = (T*)Q_renderObj->getMappedPointer(); //This cast is not entirely legit
          u_d = (T*)U_renderObj->getMappedPointer();
          Rho_d = (T*)Rho_renderObj->getMappedPointer();
        }

      }

      timeIndex++;

    }


    cudaDeviceSynchronize();
    if(cudaMemcpy(u_avg, u_d_avg, 3*L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess)
    {
      std::cout << "Failed to copy memory u_d_avg, error: " << '\n';
      getError();
    }

    cudaDeviceSynchronize();
    if(cudaMemcpy(u_std, u_d_std, 3*L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess)
    {
      std::cout << "Failed to copy memory u_d_std, error: " << '\n';
      getError();
    }

    //Divide all the elements to create the correct averaged quantity.
    for(int sw = 0; sw < L3; sw++)
    {
      u_avg[sw] = u_avg[sw]/((T) nAvg);
      u_avg[sw+L3*1] = u_avg[sw+L3*1]/((T) nAvg);
      u_avg[sw+L3*2] = u_avg[sw+L3*2]/((T) nAvg);

      //Implemented according to wikipedias definition of variance.
      u_std[sw] = sqrtf(abs(u_std[sw]/((T) nAvg) - u_avg[sw]*u_avg[sw]));
      u_std[sw+L3*1] = sqrtf(abs(u_std[sw+L3*1]/((T) nAvg) - u_avg[sw+L3*1]*u_avg[sw+L3*1]));
      u_std[sw+L3*2] = sqrtf(abs(u_std[sw+L3*2]/((T) nAvg) - u_avg[sw+L3*2]*u_avg[sw+L3*2]));

    }

    cudaDeviceSynchronize();
    if(cudaMemcpy(rho_avg, rho_d_avg, L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess)
    {
      std::cout << "Failed to copy memory rho_d_avg, error: " << '\n';
      getError();
    }

    //Divide all the elements to create the correct averaged quantity.
    for(int sw = 0; sw < L3; sw++)
    {
      rho_avg[sw] = rho_avg[sw]/((T) nAvg);
    }

    cudaDeviceSynchronize();
    if(cudaMemcpy(u, u_d, 3*L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess)
    {
      std::cout << "Failed to copy memory u_d, error: " << '\n';
      getError();
    }
    cudaDeviceSynchronize();
    if(cudaMemcpy(rho, Rho_d, L3*sizeof(T), cudaMemcpyDeviceToHost) != cudaSuccess)
    {
      std::cout << "Failed to copy memory Rho_d, error: " << '\n';
      getError();
    }
    cudaDeviceSynchronize();
    

    //Zero the elements in the u_avg again.
    for(int sw = 0; sw < L3; sw++)
    {
      u_avg[sw] = 0.0f;
      u_avg[sw+L3*1] = 0.0f;
      u_avg[sw+L3*2] = 0.0f;

      u_std[sw] = 0.0f;
      u_std[sw+L3*1] = 0.0f;
      u_std[sw+L3*2] = 0.0f;

      rho_avg[sw] = 0.0f;
    }



    cudaMemcpy(u_d_avg, u_avg, L3*3*sizeof(T), cudaMemcpyHostToDevice);
    cudaMemcpy(u_d_std, u_std, L3*3*sizeof(T), cudaMemcpyHostToDevice);
    cudaMemcpy(rho_d_avg, rho_avg, L3*sizeof(T), cudaMemcpyHostToDevice);
    nAvg = 0;
    cudaDeviceSynchronize();

    //Update the log.
    logHandle->endSweep(nVisc, k+1, (46.0f*vel/((2.0f*phi+10.0f)/12.0f))/(visc[k]/3.0f), visc[k]/3.0f, vel);

    //Delete all the points.
    exportPoints.clear();

    timeIndex = 0;

  }

  logHandle->endSession();

  cudaFree(f1_d);
  cudaFree(f2_d);
  cudaFree(wall_d);

  delete[] u;
  delete[] rho;
  delete[] u_avg;
  delete[] wall;

  return 0;

}
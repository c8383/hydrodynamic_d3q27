#version 460
in vec3 pos;

in float u;
in float v;
in float w;

uniform mat4 MVP;
uniform float strength;

out vec4 colorParticle;

void main()
{
  gl_Position = MVP*(vec4( pos.x, pos.y, pos.z, 1 ));
  //colorParticle = vec4(1.0f,1.0f,1.0f,1.0f);
  if(u == 0.0f)
  {
    colorParticle = 0.0f*vec4(1.0f,1.0f,1.0f,0.1f)*(sin(pos.x*2.0f)+sin(pos.y*1.0f)+sin(pos.z*3.0f)+4.0f+pos.z/63.0f);
  }
  else
  {
    colorParticle = strength*vec4(10*u,10*v,10*w,0.1f);
  }

}

#version 460

in vec3 pos;
in float Rho;
uniform mat4 MVP;
uniform float strength;

out vec4 colorParticle;

void main()
{
  gl_Position = MVP*(vec4( pos.x, pos.y, pos.z, 1 ));
  colorParticle = strength*vec4(1.0-Rho,1.0-Rho,1.0-Rho,0.1f);
}

/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/




#include "calc.h"
#include "../../Kernels/D3Q27_hydrodynamic/MRT/D3Q27_BC.h"
#include "../../Kernels/Utils/D3/sumvel.h"


template<typename T>
void queueCollideAndStreamURho(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, T ax, T ay, T az, T sv, T *f1_d, T *f2_d, T *u_d, T *rho_d, char *wall_d)
{
  collideAndStreamURho<T><<<*grid, *block>>>(L3, L2, Lx, Ly, Lz, xOff, yOff, zOff, ax, ay, az, sv, f1_d, f2_d, u_d, rho_d, wall_d);
}

template<typename T>
void queueCollideAndStream(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, T ax, T ay, T az, T sv, T *f1_d, T *f2_d, char *wall_d)
{
  collideAndStream<T><<<*grid, *block>>>(L3, L2, Lx, Ly, Lz, xOff, yOff, zOff, ax, ay, az, sv, f1_d, f2_d, wall_d);
}


//BC conditions
template<typename T>
void queueSetVel(dim3* block, dim3* grid, uint L3, uint start, uint offx, uint offy, T uvel, T *f1, char* wall1)
{
  setVel<T><<<*grid, *block>>>(L3, start, offx, offy, uvel, f1, wall1);
}

template<typename T>
void queueWallZP(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
{
  applyWallZP<T><<<*grid, *block>>>(L3,Lx,Ly,Lz,f1,f2);
}

template<typename T>
void queueWallZM(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
{
  applyWallZM<T><<<*grid, *block>>>(L3,Lx,Ly,Lz,f1,f2);
}

template<typename T>
void queueWallYP(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
{
  applyWallYP<T><<<*grid, *block>>>(L3,Lx,Ly,Lz,f1,f2);
}

template<typename T>
void queueWallYM(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
{
  applyWallYM<T><<<*grid, *block>>>(L3,Lx,Ly,Lz,f1,f2);
}

//GPU analysis and data saving functions.
template<typename T>
void queueSumVelAll(int arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelAll<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelU(int arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelU<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelAvg(int arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelAvg<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelRS(uint arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelRS<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelPV(uint arrLength, T *u_d, T *p_d, T *sumArray_d, uint L3)
{
  sumVelPV<T><<<arrLength, sumBlockSize>>>(u_d, p_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelVUU(uint arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelVUU<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumRhoAvg(int arrLength, T *rho_d, T *sumArray_d, uint L3)
{
  sumRhoAvg<T><<<arrLength, sumBlockSize>>>(rho_d, sumArray_d, L3);
}

template<typename T>
void queueSumTempRS(uint arrLength, T *u_d, T *t_d, T *sumArray_d, uint L3)
{
  sumTempRS<T><<<arrLength, sumBlockSize>>>(u_d, t_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelStd(uint arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelStd<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}


//Function templates
//Doubles
template void queueCollideAndStreamURho<double>(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, double ax, double ay, double az, double sv, double *f1_d, double *f2_d, double *u_d, double *rho_d, char *wall_d);
template void queueCollideAndStream<double>(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, double ax, double ay, double az, double sv, double *f1_d, double *f2_d, char *wall_d);

template void queueSumVelAll<double>(int arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelU<double>(int arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelAvg<double>(int arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelRS<double>(uint arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelPV<double>(uint arrLength, double *u_d, double *p_d, double *sumArray_d, uint L3);
template void queueSumVelVUU<double>(uint arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumRhoAvg<double>(int arrLength, double *rho_d, double *sumArray_d, uint L3);
template void queueSumTempRS<double>(uint arrLength, double *u_d, double *t_d, double *sumArray_d, uint L3);
template void queueSumVelStd<double>(uint arrLength, double *u_d, double *sumArray_d, uint L3);

template void queueSetVel<double>(dim3* block, dim3* grid, uint L3, uint start, uint offx, uint offy, double uvel, double *f1, char* wall1);
template void queueWallZP<double>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);
template void queueWallZM<double>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);
template void queueWallYP<double>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);
template void queueWallYM<double>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);

//Singles
template void queueCollideAndStreamURho<float>(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, float ax, float ay, float az, float sv, float *f1_d, float *f2_d, float *u_d, float *rho_d, char *wall_d);
template void queueCollideAndStream<float>(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, float ax, float ay, float az, float sv, float *f1_d, float *f2_d, char *wall_d);

template void queueSumVelAll<float>(int arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelU<float>(int arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelAvg<float>(int arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelRS<float>(uint arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelPV<float>(uint arrLength, float *u_d, float *p_d, float *sumArray_d, uint L3);
template void queueSumVelVUU<float>(uint arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumRhoAvg<float>(int arrLength, float *rho_d, float *sumArray_d, uint L3);
template void queueSumTempRS<float>(uint arrLength, float *u_d, float *t_d, float *sumArray_d, uint L3);
template void queueSumVelStd<float>(uint arrLength, float *u_d, float *sumArray_d, uint L3);

template void queueSetVel<float>(dim3* block, dim3* grid, uint L3, uint start, uint offx, uint offy, float uvel, float *f1, char* wall1);
template void queueWallZP<float>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
template void queueWallZM<float>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
template void queueWallYP<float>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
template void queueWallYM<float>(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
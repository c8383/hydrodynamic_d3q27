#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 21:44:46 2022

@author: tobias-home
"""

import numpy as np
import matplotlib.pyplot as plt


points = np.zeros((65,1000,3))

for i in range(65):
    points[i,:,:] = np.loadtxt('porePoint_' + str(i) + '_0.pdat')
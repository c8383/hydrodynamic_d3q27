/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/




#include <cuda_runtime.h>

template<typename T>
void queueCollideAndStreamURho(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, T ax, T ay, T az, T sv, T *f1_d, T *f2_d, T *u_d, T *rho_d, char *wall_d);

template<typename T>
void queueCollideAndStream(dim3* block, dim3* grid, uint L3, uint L2, uint Lx, uint Ly, uint Lz, uint xOff, uint yOff, uint zOff, T ax, T ay, T az, T sv, T *f1_d, T *f2_d, char *wall_d);

template<typename T>
void queueSetVel(dim3* block, dim3* grid, uint L3, uint start, uint offx, uint offy, T uvel, T *f1, char* wall1);

template<typename T>
void queueWallZP(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
void queueWallZM(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
void queueWallYP(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
void queueWallYM(dim3* block, dim3* grid, uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
void queueSumVelAll(int arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelU(int arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelAvg(int arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelRS(uint arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelPV(uint arrLength, T *u_d, T *p_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelVUU(uint arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumRhoAvg(int arrLength, T *rho_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumTempRS(uint arrLength, T *u_d, T *t_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelStd(uint arrLength, T *u_d, T *sumArray_d, uint L3);

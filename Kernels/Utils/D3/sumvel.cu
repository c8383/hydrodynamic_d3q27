/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/thermal-mrt-lbm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "sumvel.h"

/**
 * This function takes an array as an input and sums all values using quick parallel reduction.
 *
 * The function is inspired by "https://gist.github.com/wh5a/4424992", credit goes to github user wh5a
 */
template<typename T>
__global__ void sumVelU(T * input, T * output, uint size)
{
  //Set up the shared memory for the block.
  __shared__ T partialSum[2 * sumBlockSize];
  uint pos = threadIdx.x;
  uint start = 2 * blockIdx.x * sumBlockSize;

  //Check if we are within the region of interest, if the blocks evenly divide the
  //total amount of values to be summed this stage isn't necessary.
  if(start + pos < size)
  {
    partialSum[pos] = input[start+pos];
  }
  else
  {
    partialSum[pos] = 0.0f;
  }

  if(start + sumBlockSize + pos < size)
  {
    partialSum[sumBlockSize+pos] = input[start+sumBlockSize+pos];
  }
  else
  {
    partialSum[sumBlockSize+pos] = 0.0f;
  }

  //Down the reduction tree.
  for(uint stride = sumBlockSize; stride >= 1; stride >>= 1)
  {
    __syncthreads();
    if(pos < stride)
    {
      partialSum[pos] += partialSum[pos+stride];
    }
  }

  if(pos == 0)
  {
    output[blockIdx.x] = partialSum[0];
  }

}

template<typename T>
__global__ void sumVelAll(T * input, T * output, uint size)
{
  //Set up the shared memory for the block.
  __shared__ T partialSum[2 * sumBlockSize];
  uint pos = threadIdx.x;
  uint start = 2 * blockIdx.x * sumBlockSize;

  //Check if we are within the region of interest, if the blocks evenly divide the
  //total amount of values to be summed this stage isn't necessary.

  if(start + pos < size)
  {
    T ux = input[start+pos];
    T uy = input[start+pos+size];
    T uz = input[start+pos+2*size];

    partialSum[pos] = sqrt(ux*ux+uy*uy+uz*uz);
  }
  else
  {
    partialSum[pos] = 0.0f;
  }

  if(start + sumBlockSize + pos < size)
  {
    T ux = input[start+sumBlockSize+pos];
    T uy = input[start+sumBlockSize+pos+size];
    T uz = input[start+sumBlockSize+pos+2*size];

    partialSum[sumBlockSize+pos] = sqrt(ux*ux+uy*uy+uz*uz);
  }
  else
  {
    partialSum[sumBlockSize+pos] = 0.0f;
  }

  //Down the reduction tree.
  for(uint stride = sumBlockSize; stride >= 1; stride >>= 1)
  {
    __syncthreads();
    if(pos < stride)
    {
      partialSum[pos] += partialSum[pos+stride];
    }
  }

  if(pos == 0)
  {
    output[blockIdx.x] = partialSum[0];
  }

}

/*
* This function adds the velocities, the user needs to keep track of the amount of
* additions in order to get the correct scaling on the average.
*/
template<typename T> __global__ void sumVelAvg(T * input, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;

  T ux = output[pos]+input[pos];
  T uy = output[pos+size]+input[pos+size];
  T uz = output[pos+2*size]+input[pos+2*size];

  output[pos] = ux;
  output[pos+size] = uy;
  output[pos+2*size] = uz;

}

//This function simply adds up all the velocities, the user needs to keep track of the amount of
//additions in order to get the correct scaling on the average.
template<typename T> __global__ void sumRhoAvg(T * input, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;
  T rho = output[pos]+input[pos];
  output[pos] = rho;
}


//This function adds the velocity under the condition of squaring the individual components, together with
//the average this can be used to form the standard deviation.
template<typename T> __global__ void sumVelStd(T * input, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;

  T v_ux = output[pos]+input[pos]*input[pos];
  T v_uy = output[pos+size]+input[pos+size]*input[pos+size];
  T v_uz = output[pos+2*size]+input[pos+2*size]*input[pos+2*size];

  output[pos] = v_ux;
  output[pos+size] = v_uy;
  output[pos+2*size] = v_uz;

}

//This function adds the velocity under the condition of squaring the individual components, together with
//the average this can be used to form the Reynolds stresses.
template<typename T> __global__ void sumVelRS(T * input, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;

  T v_ux = output[pos]+input[pos]*input[pos];
  T v_uy = output[pos+3*size]+input[pos+size]*input[pos+size];
  T v_uz = output[pos+5*size]+input[pos+2*size]*input[pos+2*size];
  T v_uxy = output[pos+1*size]+input[pos+size]*input[pos];
  T v_uxz = output[pos+2*size]+input[pos+2*size]*input[pos];
  T v_uyz = output[pos+4*size]+input[pos+size]*input[pos+2*size];

  output[pos] = v_ux;
  output[pos+size] = v_uxy;
  output[pos+2*size] = v_uxz;
  output[pos+3*size] = v_uy;
  output[pos+4*size] = v_uyz;
  output[pos+5*size] = v_uz;

}


//This function adds the velocity under the condition of squaring the individual components, together with
//the average this can be used to form the Reynolds stresses.
template<typename T> __global__ void sumVelPV(T * input, T* input_p, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;

  //The output is in general three-dimensional, but we are only interested in the v-component for now.
  T v_ux = output[pos]+input[pos+2*size]*input_p[pos];


  output[pos] = v_ux;

}


//This function adds the velocity under the condition of squaring the individual components, together with
//the average this can be used to form the Reynolds stresses.
template<typename T> __global__ void sumVelVUU(T * input, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;

  //The output is one-dimensional but takes many inputs.
  T Vyjj = output[pos]+input[pos+2*size]*(input[pos]*input[pos]+input[pos+size]*input[pos+size]+input[pos+2*size]*input[pos+2*size]);

  output[pos] = Vyjj;

}


//This function handles all the RMS statistics of the thermal fluctuations, the v't' is not included.
template<typename T> __global__ void sumTempRS(T * inputU, T * inputT, T * output, uint size)
{
  //We use the same sumBlockSize, but really the blocksize is irrelevant.
  uint pos = threadIdx.x+blockIdx.x*blockDim.x;

  T tt = output[pos]+inputT[pos]*inputT[pos];
  T tu = output[pos+1*size]+inputT[pos]*inputU[pos];
  T tw = output[pos+2*size]+inputT[pos]*inputU[pos+2*size];

  output[pos] = tt;
  output[pos+size] = tu;
  output[pos+2*size] = tw;

}



template __global__ void sumVelAll<double>(double * input, double * output, uint size);
template __global__ void sumVelU<double>(double * input, double * output, uint size);
template __global__ void sumVelAvg<double>(double * input, double * output, uint size);
template __global__ void sumRhoAvg<double>(double * input, double * output, uint size);
template __global__ void sumVelStd<double>(double * input, double * output, uint size);
template __global__ void sumVelRS<double>(double * input, double * output, uint size);
template __global__ void sumVelPV<double>(double * input, double * input_p, double * output, uint size);
template __global__ void sumVelVUU<double>(double * input, double * output, uint size);
template __global__ void sumTempRS<double>(double * inputU, double * inputT, double * output, uint size);

template __global__ void sumVelAll<float>(float * input, float * output, uint size);
template __global__ void sumVelU<float>(float * input, float * output, uint size);
template __global__ void sumVelAvg<float>(float * input, float * output, uint size);
template __global__ void sumRhoAvg<float>(float * input, float * output, uint size);
template __global__ void sumVelStd<float>(float * input, float * output, uint size);
template __global__ void sumVelRS<float>(float * input, float * output, uint size);
template __global__ void sumVelPV<float>(float * input, float * input_p, float * output, uint size);
template __global__ void sumVelVUU<float>(float * input, float * output, uint size);
template __global__ void sumTempRS<float>(float * inputU, float * inputT, float * output, uint size);

/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <cuda_runtime.h>

//Weights
#define w1 0.004629629629629629f
#define w2 0.018518518518518517f
#define w3 0.004629629629629629f
#define w4 0.018518518518518517f
#define w5 0.07407407407407407f
#define w6 0.018518518518518517f
#define w7 0.004629629629629629f
#define w8 0.018518518518518517f
#define w9 0.004629629629629629f
#define w10 0.018518518518518517f
#define w11 0.07407407407407407f
#define w12 0.018518518518518517f
#define w13 0.07407407407407407f
#define w14 0.2962962962962963f
#define w15 0.07407407407407407f
#define w16 0.018518518518518517f
#define w17 0.07407407407407407f
#define w18 0.018518518518518517f
#define w19 0.004629629629629629f
#define w20 0.018518518518518517f
#define w21 0.004629629629629629f
#define w22 0.018518518518518517f
#define w23 0.07407407407407407f
#define w24 0.018518518518518517f
#define w25 0.004629629629629629f
#define w26 0.018518518518518517f
#define w27 0.004629629629629629f

//these values are defined in the file above this one.
#define cs 0.3333333f //This is cs^2
#define const_C 1.0f

//The relaxation factors.
#define rFac9 1.54f
#define rFac10 1.54f
#define rFac11 1.54f
#define rFac12 1.54f
#define rFac13 1.99f
#define rFac14 1.99f
#define rFac15 1.99f
#define rFac16 1.99f
#define rFac17 1.45f
#define rFac18 1.99f
#define rFac19 1.99f
#define rFac20 1.99f
#define rFac21 1.99f
#define rFac22 1.99f
#define rFac23 1.74f
#define rFac24 1.74f
#define rFac25 1.74f
#define rFac26 1.74f


template<typename T>
__global__ void reNormalize(T *fi, uint L3, uint Lx, T avgRho);

//Base streaming and collision functions.
template<typename T>
__global__ void collideAndStream(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                  uint xOff, uint yOff, uint zOff,
                                  T ax, T ay, T az, T sv,
                                  T *f1, T *f2, char *wall1);

//Stream and save all variables.
template<typename T>
__global__ void collideAndStreamURhoQ(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                  uint xOff, uint yOff, uint zOff,
                                  T ax, T ay, T az, T sv,
                                  T *f1, T *f2, T *ug, T *rhog, T *Qg, char *wall1);


//BC functions.

template<typename T>
__global__ void setVel(uint L3, uint start, uint offx, uint offy, T uvel, T *f1, char *wall1);

template<typename T>
__global__ void zeroGradXM(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    T ax, T ay, T az, T sv,
                                    T *f1, T *f2, T *ug, T *rhog, char *wall1);

template<typename T>
__global__ void applyWallZP(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
__global__ void applyWallZM(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
__global__ void applyWallYP(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);

template<typename T>
__global__ void applyWallYM(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2);
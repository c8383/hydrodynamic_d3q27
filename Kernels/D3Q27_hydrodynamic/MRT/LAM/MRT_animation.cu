/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "../D3Q27_BC_animation.h"


template<typename T>
__global__ void collideAndStream(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                  uint xOff, uint yOff, uint zOff,
                                  T ax, T ay, T az, T sv,
                                  T *f1, T *f2, char *wall1)
{
  //Get position.
  uint x = blockIdx.x*blockDim.x+threadIdx.x+xOff;
  uint y = blockIdx.y*blockDim.y+threadIdx.y+yOff;
  uint z = blockIdx.z*blockDim.z+threadIdx.z+zOff;

  uint pos = (x)+(y)*Lx+(z)*Lx*Ly;

  //Calculate neighbour positions

  uint xp = (x+1+Lx)%Lx;
  uint xm = (x-1+Lx)%Lx;
  uint yp = (y+1+Ly)%Ly;
  uint ym = (y-1+Ly)%Ly;
  uint zp = (z+1+Lz)%Lz;
  uint zm = (z-1+Lz)%Lz;

  //Copy to the local memory.
  char wall = wall1[pos];
  T f[27];

  if(wall == '0')
  {
    T wc[27];
    T fc[27];
    
    //Do non wall-streaming.
    #include "Stream.cu"

    //Run the collision operator.
    #include "cOperator_auto.cu"

    //Copy back to position.
    #include "Transfer.cu"

  }
  else
  {

    //Do wall streaming.
    #include "WallStream.cu"

    //Run the collision operator, none for now.

    //Now copy the results to the array.
    #include "WallTransfer.cu"
    
  }

}


template<typename T>
__global__ void collideAndStreamURhoQ(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                  uint xOff, uint yOff, uint zOff,
                                  T ax, T ay, T az, T sv, 
                                  T *f1, T *f2, T *ug, T *rhog, T *Qg, char *wall1)
{
  //Figure out where you are using the new method, this is the only part that changes with changing
  //periodic conditions.
  uint x = blockIdx.x*blockDim.x+threadIdx.x+xOff;
  uint y = blockIdx.y*blockDim.y+threadIdx.y+yOff;
  uint z = blockIdx.z*blockDim.z+threadIdx.z+zOff;

  uint pos = (x)+(y)*Lx+(z)*Lx*Ly;

  //Calculate neighbour positions

  uint xp = (x+1+Lx)%Lx;
  uint xm = (x-1+Lx)%Lx;
  uint yp = (y+1+Ly)%Ly;
  uint ym = (y-1+Ly)%Ly;
  uint zp = (z+1+Lz)%Lz;
  uint zm = (z-1+Lz)%Lz;

  //Copy to the local memory.
  char wall = wall1[pos];
  T f[27];

  if(wall == '0')
  {

    T wc[27];
    T fc[27];
    T Q;

    //Do non wall-streaming.
    #include "Stream.cu"

    //Run the collision operator.
    #include "cOperator_auto_animation.cu"

    //Copy back to position.
    #include "Transfer.cu"
    
    //The absolute value of the vorticity.
    Qg[pos] = Q;

    //Body force is removed after calculation
    ug[pos] = u[0];
    ug[pos+L3] = u[1];
    ug[pos+2*L3] = u[2];

    //Also export the density.
    rhog[pos] = rho;

  }
  else
  {

    //Do wall streaming.
    #include "WallStream.cu"

    //Run the collision operator.
    //#include "cOperator_wall_channel.cu"

    //Now copy the results to the array.
    #include "WallTransfer.cu"

    Qg[pos] = 0.0f;

    //Velocity transfers.
    ug[pos] = 0.0f;
    ug[pos+L3] = 0.0f;
    ug[pos+2*L3] = 0.0f;

    //Also put the density to exactly on for walls.
    rhog[pos] = 1.0f;

  }

}


//Boundary conditions.
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------




//This function applies a zero gradient condition in the velocity field at the positive x edge, runs inefficeintly due to collocation of memory.
template<typename T>
__global__ void zeroGradXM(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    T ax, T ay, T az, T sv,
                                    T *f1, T *f2, T *ug, T *rhog, char *wall1)
{
    //Figure out where you are using the new method.
    uint x = blockIdx.x*blockDim.x+threadIdx.x+xOff;
    uint y = blockIdx.y*blockDim.y+threadIdx.y+yOff;
    uint z =  blockIdx.z*blockDim.z+threadIdx.z+zOff;

    uint pos = (x)+(y)*Lx+(z)*Lxy;

    //Calculate neighbour positions
    uint xp = (x+1+Lx-1)%Lx;
    uint xm = (x-1+Lx-1)%Lx;
    uint yp = (y+1+Ly)%Ly;
    uint ym = (y-1+Ly)%Ly;
    uint zp = (z+1+Lz)%Lz;
    uint zm = (z-1+Lz)%Lz;

    //Copy to the local memory.
    char wall = wall1[pos];
    T f[27];

    if(wall == '0')
    {
      T wc[27];
      T fc[27];
      
      //Do non wall-streaming.
      #include "Stream.cu"

      //Run the collision operator.
      #include "cOperator_auto.cu"

      //Copy back to position.
      #include "Transfer.cu"

    }
    else
    {

      //Do wall streaming.
      #include "WallStream.cu"

      //Run the collision operator.
      //#include "cOperator_wall_channel.cu"

      //Now copy the results to the array.
      #include "WallTransfer.cu"
      
    }

}


template<typename T> __global__ void setVel(uint L3, uint start, uint offx, uint offy, T uvel, T *f1, char* wall1)
{
  //Figure out where you are using the new method.
  uint x = blockIdx.x*blockDim.x+threadIdx.x;
  uint y = blockIdx.y*blockDim.y+threadIdx.y;
  uint pos = start+offx*x+offy*y;

  //Copy to the local memory.
  char wall = wall1[pos];

  //Only apply this operation on fluid elements
  if(wall == '0')
  {

    //Maybe I should read in the distribution first?


    //First find the values of the distribution functions by calculating them, rho should hover around 1.
    T f[27];

    T rho = 1.0;

    T u[3];
    u[0] = (const_C/rho) * (uvel);
    u[1] = 0.0;
    u[2] = 0.0;


    T eiu;
    T uu2c;

    
    //Thos code should be adapted as to correctly handle the BC.
    uu2c = (uvel*uvel) * 1.5f / (const_C * const_C);

    eiu = -u[0]-u[1]-u[2];
    f[0] = w1*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[1]-u[2];
    f[1] = w2*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]-u[1]-u[2];
    f[2] = w3*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]-u[2];
    f[3] = w4*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[2];
    f[4] = w5*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]-u[2];
    f[5] = w6*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]+u[1]-u[2];
    f[6] = w7*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[1]-u[2];
    f[7] = w8*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]+u[1]-u[2];
    f[8] = w9*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]-u[1];
    f[9] = w10*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[1];
    f[10] = w11*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]-u[1];
    f[11] = w12*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0];
    f[12] = w13*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    f[13] = w14*rho*( - uu2c + 1.0f );

    eiu = u[0];
    f[14] = w15*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]+u[1];
    f[15] = w16*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[1];
    f[16] = w17*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]+u[1];
    f[17] = w18*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]-u[1]+u[2];
    f[18] = w19*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[1]+u[2];
    f[19] = w20*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]-u[1]+u[2];
    f[20] = w21*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]+u[2];
    f[21] = w22*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[2];
    f[22] = w23*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]+u[2];
    f[23] = w24*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = -u[0]+u[1]+u[2];
    f[24] = w25*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[1]+u[2];
    f[25] = w26*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    eiu = u[0]+u[1]+u[2];
    f[26] = w27*rho*((3.0f * eiu / const_C) + (4.5f * eiu * eiu / (const_C * const_C)) - uu2c + 1.0f);

    //Transfer these values to the lattice pouints of uinterest.
    f1[pos] = f[0];
    f1[pos+L3*1] = f[1];
    f1[pos+L3*2] = f[2];
    f1[pos+L3*3] = f[3];
    f1[pos+L3*4] = f[4];
    f1[pos+L3*5] = f[5];
    f1[pos+L3*6] = f[6];
    f1[pos+L3*7] = f[7];
    f1[pos+L3*8] = f[8];
    f1[pos+L3*9] = f[9];
    f1[pos+L3*10] = f[10];
    f1[pos+L3*11] = f[11];
    f1[pos+L3*12] = f[12];
    f1[pos+L3*13] = f[13];
    f1[pos+L3*14] = f[14];
    f1[pos+L3*15] = f[15];
    f1[pos+L3*16] = f[16];
    f1[pos+L3*17] = f[17];
    f1[pos+L3*18] = f[18];
    f1[pos+L3*19] = f[19];
    f1[pos+L3*20] = f[20];
    f1[pos+L3*21] = f[21];
    f1[pos+L3*22] = f[22];
    f1[pos+L3*23] = f[23];
    f1[pos+L3*24] = f[24];
    f1[pos+L3*25] = f[25];
    f1[pos+L3*26] = f[26];
  }

}


/**
 * This function applies a wall condition on the z-top boundary.
 */
 template<typename T> __global__ void applyWallZP(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
 {
   uint x = blockIdx.x*blockDim.x+threadIdx.x;
   uint y = blockIdx.y*blockDim.y+threadIdx.y;
   uint z = Lz-1;
 
   uint Lxy = (Lx)*(Ly);
   uint pos = x+y*Lx+Lxy*z;
 
   //Since this is the plus direction.
   uint xp = (x+1+Lx)%(Lx);
   uint xm = (x-1+Lx)%(Lx);
   uint yp = (y+1+Ly)%(Ly);
   uint ym = (y-1+Ly)%(Ly);
   uint zm = Lz-2; //This is a constant value for this case.
 
   //I need a middle storage, maybe not but do this first at least.
   T f[27];
 
   //All the values at the top of the lower layer should be streamed uinto the bottom layer.
   f[8] = f1[L3*18+xp+yp*Lx+zm*Lxy];
   f[7] = f1[L3*19+x+yp*Lx+zm*Lxy];
   f[6] = f1[L3*20+xm+yp*Lx+zm*Lxy];
   f[5] = f1[L3*21+xp+y*Lx+zm*Lxy];
   f[4] = f1[L3*22+x+y*Lx+zm*Lxy];
   f[3] = f1[L3*23+xm+y*Lx+zm*Lxy];
   f[2] = f1[L3*24+xp+ym*Lx+zm*Lxy];
   f[1] = f1[L3*25+x+ym*Lx+zm*Lxy];
   f[0] = f1[L3*26+xm+ym*Lx+zm*Lxy];
 
   //Now stream this back uinto the funct.
   f2[pos] = f[0];
   f2[pos+L3*1] = f[1];
   f2[pos+L3*2] = f[2];
   f2[pos+L3*3] = f[3];
   f2[pos+L3*4] = f[4];
   f2[pos+L3*5] = f[5];
   f2[pos+L3*6] = f[6];
   f2[pos+L3*7] = f[7];
   f2[pos+L3*8] = f[8];
 
   //There should not be any more than this?
 }
 
 
 /**
  * This function applies a wall condition on the z-bottom boundary.
  */
 template<typename T> __global__ void applyWallZM(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
 {
   uint x = blockIdx.x*blockDim.x+threadIdx.x;
   uint y = blockIdx.y*blockDim.y+threadIdx.y;
   uint z = 0;
 
   uint Lxy = (Lx)*(Ly);
   uint pos = x+y*Lx+Lxy*z;
 
   //Since this is the plus direction.
   uint xp = (x+1+Lx)%(Lx);
   uint xm = (x-1+Lx)%(Lx);
   uint yp = (y+1+Ly)%(Ly);
   uint ym = (y-1+Ly)%(Ly);
   uint zp = 1; //This is a constant value for this case.
 
   //I need a middle storage, maybe not but do this first at least.
   T f[27];
 
   //All the values at the top of the lower layer should be streamed uinto the bottom layer.
   f[26] = f1[xp+yp*Lx+zp*Lxy];
   f[25] = f1[L3*1+x+yp*Lx+zp*Lxy];
   f[24] = f1[L3*2+xm+yp*Lx+zp*Lxy];
   f[23] = f1[L3*3+xp+y*Lx+zp*Lxy];
   f[22] = f1[L3*4+x+y*Lx+zp*Lxy];
   f[21] = f1[L3*5+xm+y*Lx+zp*Lxy];
   f[20] = f1[L3*6+xp+ym*Lx+zp*Lxy];
   f[19] = f1[L3*7+x+ym*Lx+zp*Lxy];
   f[18] = f1[L3*8+xm+ym*Lx+zp*Lxy];
 
   //Now stream this back uinto the funct.
   f2[pos+L3*18] = f[18];
   f2[pos+L3*19] = f[19];
   f2[pos+L3*20] = f[20];
   f2[pos+L3*21] = f[21];
   f2[pos+L3*22] = f[22];
   f2[pos+L3*23] = f[23];
   f2[pos+L3*24] = f[24];
   f2[pos+L3*25] = f[25];
   f2[pos+L3*26] = f[26];
 
   //There should not be any more than this?
 }
 
 /**
  * This function applies a wall condition on the y-top boundary.
  */
 template<typename T> __global__ void applyWallYP(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
 {
   uint x = blockIdx.x*blockDim.x+threadIdx.x;
   uint y = Ly-1;
   uint z = blockIdx.y*blockDim.y+threadIdx.y;
 
   uint Lxy = (Lx)*(Ly);
   uint pos = x+y*Lx+Lxy*z;
 
   //Since this is the plus direction.
   uint xp = (x+1+Lx)%(Lx);
   uint xm = (x-1+Lx)%(Lx);
   uint zp = (z+1+Lz)%(Lz);
   uint zm = (z-1+Lz)%(Lz);
   uint ym = Ly-2; //This is a constant value for this case.
 
   //I need a middle storage, maybe not but do this first at least.
   T f[27];
 
   //All the values at the top of the lower layer should be streamed uinto the bottom layer.
   f[20] = f1[L3*6+xp+ym*Lx+zp*Lxy];
   f[19] = f1[L3*7+x+ym*Lx+zp*Lxy];
   f[18] = f1[L3*8+xm+ym*Lx+zp*Lxy];
   f[11] = f1[L3*15+xp+ym*Lx+z*Lxy];
   f[10] = f1[L3*16+x+ym*Lx+z*Lxy];
   f[9] = f1[L3*17+xm+ym*Lx+z*Lxy];
   f[2] = f1[L3*24+xp+ym*Lx+zm*Lxy];
   f[1] = f1[L3*25+x+ym*Lx+zm*Lxy];
   f[0] = f1[L3*26+xm+ym*Lx+zm*Lxy];
 
   //Now stream this back uinto the funct.
   f2[pos+L3*20] = f[20];
   f2[pos+L3*19] = f[19];
   f2[pos+L3*18] = f[18];
   f2[pos+L3*11] = f[11];
   f2[pos+L3*10] = f[10];
   f2[pos+L3*9] = f[9];
   f2[pos+L3*2] = f[2];
   f2[pos+L3*1] = f[1];
   f2[pos+L3*0] = f[0];
 
   //There should not be any more than this?
 }
 
 
 /**
  * This function applies a wall condition on the y-bottom boundary.
  */
 template<typename T> __global__ void applyWallYM(uint L3, uint Lx, uint Ly, uint Lz, T *f1, T *f2)
 {
   uint x = blockIdx.x*blockDim.x+threadIdx.x;
   uint y = 0;
   uint z = blockIdx.y*blockDim.y+threadIdx.y;
 
   uint Lxy = (Lx)*(Ly);
   uint pos = x+y*Lx+Lxy*z;
 
   //Since this is the plus direction.
   uint xp = (x+1+Lx)%(Lx);
   uint xm = (x-1+Lx)%(Lx);
   uint zp = (z+1+Lz)%(Lz);
   uint zm = (z-1+Lz)%(Lz);
   uint yp = 1; //This is a constant value for this case.
 
   //I need a middle storage, maybe not but do this first at least.
   T f[27];
 
   //All the values at the top of the lower layer should be streamed uinto the bottom layer.
   //Do wall streaming.
   f[26] = f1[xp+yp*Lx+zp*Lxy];
   f[25] = f1[L3*1+x+yp*Lx+zp*Lxy];
   f[24] = f1[L3*2+xm+yp*Lx+zp*Lxy];
   f[17] = f1[L3*9+xp+yp*Lx+z*Lxy];
   f[16] = f1[L3*10+x+yp*Lx+z*Lxy];
   f[15] = f1[L3*11+xm+yp*Lx+z*Lxy];
   f[8] = f1[L3*18+xp+yp*Lx+zm*Lxy];
   f[7] = f1[L3*19+x+yp*Lx+zm*Lxy];
   f[6] = f1[L3*20+xm+yp*Lx+zm*Lxy];
 
   //Now stream this back uinto the funct.
   f2[pos+L3*26] = f[26];
   f2[pos+L3*25] = f[25];
   f2[pos+L3*24] = f[24];
   f2[pos+L3*17] = f[17];
   f2[pos+L3*16] = f[16];
   f2[pos+L3*15] = f[15];
   f2[pos+L3*8] = f[8];
   f2[pos+L3*7] = f[7];
   f2[pos+L3*6] = f[6];
 
   //There should not be any more than this?
 }
 

//Double template declares
template __global__ void collideAndStream<double>(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    double ax, double ay, double az, double sv,
                                    double *f1, double *f2, char *wall1);


template __global__ void collideAndStreamURhoQ<double>(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    double ax, double ay, double az, double sv,
                                    double *f1, double *f2, double *ug, double *rhog, double *Qg, char *wall1);


template __global__ void zeroGradXM<double>(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    double ax, double ay, double az, double sv,
                                    double *f1, double *f2, double *ug, double *rhog, char *wall1);


template __global__ void setVel<double>(uint L3, uint start, uint offx, uint offy, double uvel, double *f1, char* wall1);

//Float template declares
template __global__ void collideAndStream<float>(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz, 
                                    uint xOff, uint yOff, uint zOff,
                                    float ax, float ay, float az, float sv,
                                    float *f1, float *f2, char *wall1);


template __global__ void collideAndStreamURhoQ<float>(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    float ax, float ay, float az, float sv,
                                    float *f1, float *f2, float *ug, float *rhog, float* Qg, char *wall1);


template __global__ void zeroGradXM<float>(uint L3, uint Lxy, uint Lx, uint Ly, uint Lz,
                                    uint xOff, uint yOff, uint zOff,
                                    float ax, float ay, float az, float sv,
                                    float *f1, float *f2, float *ug, float *rhog, char *wall1);


template __global__ void setVel<float>(uint L3, uint start, uint offx, uint offy, float uvel, float *f1, char* wall1);


//Wall conditions.
template __global__ void applyWallZP<double>(uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);
template __global__ void applyWallZM<double>(uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);
template __global__ void applyWallYP<double>(uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);
template __global__ void applyWallYM<double>(uint L3, uint Lx, uint Ly, uint Lz, double *f1, double *f2);

template __global__ void applyWallZP<float>(uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
template __global__ void applyWallZM<float>(uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
template __global__ void applyWallYP<float>(uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
template __global__ void applyWallYM<float>(uint L3, uint Lx, uint Ly, uint Lz, float *f1, float *f2);
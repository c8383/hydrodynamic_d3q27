/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

//Fluid stream transfer.
f2[pos] = f[0];
f2[pos+L3*1] = f[1];
f2[pos+L3*2] = f[2];
f2[pos+L3*3] = f[3];
f2[pos+L3*4] = f[4];
f2[pos+L3*5] = f[5];
f2[pos+L3*6] = f[6];
f2[pos+L3*7] = f[7];
f2[pos+L3*8] = f[8];
f2[pos+L3*9] = f[9];
f2[pos+L3*10] = f[10];
f2[pos+L3*11] = f[11];
f2[pos+L3*12] = f[12];
f2[pos+L3*13] = f[13];
f2[pos+L3*14] = f[14];
f2[pos+L3*15] = f[15];
f2[pos+L3*16] = f[16];
f2[pos+L3*17] = f[17];
f2[pos+L3*18] = f[18];
f2[pos+L3*19] = f[19];
f2[pos+L3*20] = f[20];
f2[pos+L3*21] = f[21];
f2[pos+L3*22] = f[22];
f2[pos+L3*23] = f[23];
f2[pos+L3*24] = f[24];
f2[pos+L3*25] = f[25];
f2[pos+L3*26] = f[26];

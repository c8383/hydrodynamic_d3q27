/*

This source file is published as part of the Thermal-MRT-LBM project.

Published under MIT license:

Copyright (c) 2024 https://gitlab.com/c8383/hydrodynamic_d3q27

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

//The fluid streaming.
f[0] = f1[xp+yp*Lx+zp*Lxy];
f[1] = f1[L3*1+x+yp*Lx+zp*Lxy];
f[2] = f1[L3*2+xm+yp*Lx+zp*Lxy];
f[3] = f1[L3*3+xp+y*Lx+zp*Lxy];
f[4] = f1[L3*4+x+y*Lx+zp*Lxy];
f[5] = f1[L3*5+xm+y*Lx+zp*Lxy];
f[6] = f1[L3*6+xp+ym*Lx+zp*Lxy];
f[7] = f1[L3*7+x+ym*Lx+zp*Lxy];
f[8] = f1[L3*8+xm+ym*Lx+zp*Lxy];
f[9] = f1[L3*9+xp+yp*Lx+z*Lxy];
f[10] = f1[L3*10+x+yp*Lx+z*Lxy];
f[11] = f1[L3*11+xm+yp*Lx+z*Lxy];
f[12] = f1[L3*12+xp+y*Lx+z*Lxy];
f[13] = f1[L3*13+x+y*Lx+z*Lxy];
f[14] = f1[L3*14+xm+y*Lx+z*Lxy];
f[15] = f1[L3*15+xp+ym*Lx+z*Lxy];
f[16] = f1[L3*16+x+ym*Lx+z*Lxy];
f[17] = f1[L3*17+xm+ym*Lx+z*Lxy];
f[18] = f1[L3*18+xp+yp*Lx+zm*Lxy];
f[19] = f1[L3*19+x+yp*Lx+zm*Lxy];
f[20] = f1[L3*20+xm+yp*Lx+zm*Lxy];
f[21] = f1[L3*21+xp+y*Lx+zm*Lxy];
f[22] = f1[L3*22+x+y*Lx+zm*Lxy];
f[23] = f1[L3*23+xm+y*Lx+zm*Lxy];
f[24] = f1[L3*24+xp+ym*Lx+zm*Lxy];
f[25] = f1[L3*25+x+ym*Lx+zm*Lxy];
f[26] = f1[L3*26+xm+ym*Lx+zm*Lxy];
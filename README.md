# Hydrodynamic MRT LBM
This project is a CUDA implementation of a Multiple Relaxation Time Lattice Boltzmann Method (MRT-LBM) solver using a D3Q27 lattice for the hydrodynamic solver. The solver is coupled to a live renderer using GLEW, GLM and OpenGL to enable live feedback of the entire simulation domain.

## Prerequisites
The prerequisites for running this project is a CUDA-capable GPU with the nvidia-cuda-toolkit installed. In addition the project also makes use of the following libraries.

GLEW - http://glew.sourceforge.net/ \
GLM - https://github.com/g-truc/glm \
OpenGL - https://www.opengl.org/ \


### Aptitude installation

Using the aptitude package manager all the required packages can be installed using the command 

```
sudo apt install nvidia-cuda-toolkit libglew-dev libglm-dev mesa-common-dev
```